# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 13:28:12 2021

@author: adria036

This script selects frames for annotation and partitions them in different
folders between the annotators. The frame selection is uniform across the 
videos (resulting from S3_sampleframes_time), that cover the necessary 
variability. 

----
Extension (when time) should include loading the excel file to 
keep track of which videos are already annotated. To this end, we need to read
FrameSelection.xlsx in the source folder, and use the frame_% and vid_% name 
to indicate which frames are selected in 'fn'. 
----

"""



#%% Load packages

from datetime import date
import os
import pandas as pd
from math import ceil
from shutil import copyfile



#%% define constants and filepaths

# directory with frames (source)
src = r'W:\ASG\WLR_Dataopslag\Genomica\Sustainable_breeding//' \
        '44 0000 2700 KB DDHT AI 2020\\6. data/annotation_videos/'

#----------------------------------------------------------------------------#        
# list of video frames selected with certain criteria
### fn = [f for f in os.listdir(src) if os.path.isfile(src+f) \
###        and "frame_5.jpg" in f]

#### OR  ####

# list uniformely sampled with prespecified number
noframes = 150                        # specify approx number of frames
fn = [f for f in os.listdir(src) if os.path.isfile(src+f) \
        and ".jpg" in f]              # all .jpg files
fn.sort()                             # sort fn list
fn = fn[0:len(fn):round(len(fn)/noframes)]
#----------------------------------------------------------------------------#

# annotators
annotators = ["inesadriaens","inahulsegge"]

# prepare saving -- today's date for record
today = date.today()
today = today.strftime("%Y%m%d")

# directory to write selected frames to
framedir = []                       # var to append to
for i in range(0,len(annotators)):
    framedir.append(src + today + "_" + annotators[i] + '/')
    # create destination folder when it doesn't exists
    if os.path.exists(framedir[i]) == False:
        os.mkdir(framedir[i])
        
# excel file with meta data of the videos selected for annotation
vidfile = '20210801_videos_voor_annotatie.xlsx'
selsheet = '20210802'                           # sheet to load

# read meta data in pandas data frame
vid_list = pd.read_excel(src+vidfile,
                         sheet_name = selsheet,
                         usecols = 'B:Q')                       
vid_list.head()


#%% put annotation frames in folders
        
# make list with annotators directories length equal to number of frames
framedir_list = framedir*(ceil(len(fn)/len(annotators)))

# copy selected frames in directories
for f in range(0,len(fn)):
    copyfile(src+fn[f], framedir_list[f]+fn[f])
    

#%% save in excelfile which frames are already annotated

# 

for i in range(0,len(fn)):
    idx = fn[i].find('_',4)                 # location of second '_'
    vid = fn[i][0:idx]                      # vid
    frame = int(fn[i][idx+7:-4])            # frame number
    
    vid_list["samplename"] 
    
