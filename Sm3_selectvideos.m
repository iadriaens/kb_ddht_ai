%% S3_select_vid_newanalysis.m
%--------------------------------------------------------------------------
% Created by Ines Adriaens, December 2nd 2021
% 
% Embedded in KB DDHT Artificial intelligence
%             
%--------------------------------------------------------------------------
% This script produces a list of selected videos from a prespecified folder
% to analyse liedown and getup duration based on bb shape and location peak
%--------------------------------------------------------------------------

clear variables
close all
clc

%% Set filepaths / summary contents

% video path source
init_.inpdir = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\liestand\ses2\';

% video path destination
init_.destdir = ['W:\ASG\WLR_Genomica\Projects\Sustainable_breeding\' ...
                '44 0000 2700 KB DDHT AI 2020\6. Data\gu_ld_bb'];

% number of selected videos
N = 50;

% list 
vid_list.getup = ls([init_.inpdir 'getup*']);
vid_list.liedown = ls([init_.inpdir 'lied*']);


%% select videos and place them in new folder

% select N videos randomly from the list
sel_list.getup = vid_list.getup(sort(randperm(size(vid_list.getup,1),N)),:);
sel_list.liedown = vid_list.liedown(sort(randperm(size(vid_list.getup,1),N)),:);

% create directory if not existent
if ~isfolder(init_.destdir) % folder doesn't exist - create
    mkdir(init_.destdir)
end

% delete all that are in
delete('init_.destdir\*.avi')

% copy new selection in folder
for i = 1:N
    disp(['i = ' num2str(i) ' out of ' num2str(N)])
    system(['copy "' init_.inpdir sel_list.getup(i,:) '"'  ... 
                ' "' [init_.destdir '\' sel_list.getup(i,:)] '"']); 
%     system(['copy "' init_.inpdir sel_list.liedown(i,:) '"'  ... 
%                 ' "' [init_.destdir '\' sel_list.liedown(i,:)] '"']); 
end

    
    
    
    