# -*- coding: utf-8 -*-
"""
Created on Wed Jun 23 14:53:06 2021
@author: adria036

This script copies a file (video) into a folder. It's targeted use is to
access data stored on the W:/ drive and copy it into a local (HPC or personal)
folder.

"""

#%% Import packages and modules

from shutil import copyfile
import os

#%% Set file paths, filenames and constants -- COPY SINGLE FILE

# source directory
src = r'W:/ASG/WLR_Dataopslag/DWZ/1519/DC2019/liestand/ses1/'

# target directory
dst = r'C:/Users/adria036/OneDrive - Wageningen University & '
dst += 'Research/iAdriaens_doc/Projects/cKamphuis/ddhtAI/data/vids/'

# filename of single file
fn = 'getup_ses1_0064.avi'

# check existence
print("filepath exists = " , os.path.exists(src+fn))

# Copy file from source (src) to target (dst)
copyfile(src+fn, dst+fn)  # complete filename target

# Check success
print("filepath exists = " , os.path.exists(dst+fn))


#%% Set file paths, filenames and constants -- ALL files with conditions

# source directory
src = r'W:/ASG/WLR_Dataopslag/DWZ/1519/DC2019/ses1/video/'

# target directory
dst = r'C:/Users/adria036/OneDrive - Wageningen University & '
dst += 'Research/iAdriaens_doc/Projects/cKamphuis/ddhtAI/data/vids/'

# max number of videos to copy
maxvids = 2 

# filenames in list all .mp4 in folder, filename contains ch01_
fn = [f for f in os.listdir(src) if os.path.isfile(src+f) \
        and ".mp4" in f \
        and "ch01_" in f]

# stepsize to obtain max number of vids
steps = len(fn)//maxvids

# data reduction -- max number of videos in new list
fn1 = fn[::steps]

# copy files from src to dst
for f in range(0,len(fn1)):
    copyfile(src+fn1[f], dst+fn1[f])
