# -*- coding: utf-8 -*-
"""
Created on Wed Jun 30 19:41:13 2021

@author: adria036

This script samples frame from a video and changes its characteristics for data
    augmenentation properties, after which the 'augmented' frames/images are
    saved with the original name + extension of augmentations + adjustment 
    constants. This way, data annotations apply both on the original and the 
    augmented frames, or can be calculated from them using the constants.
    Augmentations implemented in this script:
        - brightness / light intensity adjustments
        - color adjustments
        - contrast adjustments
        - image stretches
        - image rotations
        
        
* Contrast 
    > Weber: best for uniform backgrounds + small objects
    > Michelson: bright and dark features take up similar fractions
    > Root-mean-Square contrast: standard deviation of pixel intesities
    
* Illumination > gamma corrections


Entropy is a statistical measure of randomness that can be used to 
characterize the texture of the input image. Entropy is defined as 
-sum(p.*log2(p)), where p contains the normalized histogram counts 
returned from imhist.
"""



#%% set constants and filepaths and files

# define which augmentations to apply
apply_augments = {"Contrast_WEB": 1,
                  "Contrast_MIC": 1,
                  "Contrast_RMS": 1,
                  "Ill_gamma":1,
                  }

# set range from which to sample
range_augments = {"Range_CWEB": (-5,5),
                  "Range_CMIC":(-5,5),
                  "Range_CRMS":(-5,5),
                  "Range_IGam":(-1,1)}

#%% define augmentation functions






#%% load image
#%% standardisation + grey scale image
#%% contrasts
#%% illumination with gamma correction
