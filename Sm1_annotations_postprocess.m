%% Annotation postprocessing script
% created by Ines Adriaens
%       August 3, 2021
% ------------------------------------------------------------------
% first set the constants and file paths according to your own needs
% ------------------------------------------------------------------
%
% This script:
%   1) reads the annotation results
%   2) visualises bounding boxes
%   3) saves these files in a separate folder mapfig as jpg
%   4) based on viewing of these, you can make a list 'listfail'
%   5) the 'failed' annotations will be deleted > run set_boundingboxesV3.m
%      again to redo the annotations for the 'failed' files
%   6) when all = successful and listfail = empty > writes results to
%      separate text files in yolo format


clear variables
clc

%% set filepaths and constants

% names (fill your own name first; if you want to compare bounding boxes,
% fill more names
names = {'Ina Hulsegge'};%;'Wijbrand Ouweltjes'};%'Ina Hulsegge'};
   
% manually check the plots to see which files need new annotations
% listfail = ['vid_15_frame_2.jpg ';...   % make sure add spaces for length
%             'vid_98_frame_2.jpg']; % put them in listfail
listfail = []; 

% input directory
mapin = ['W:\ASG\WLR_Dataopslag\Genomica\Sustainable_breeding\' ...
             '44 0000 2700 KB DDHT AI 2020\6. data\annotation_videos\' ...
             '20210803_inahulsegge\']; 

% result directory
mapout = ['C:\Users\adria036\OneDrive - Wageningen University & '...
          'Research\iAdriaens_doc\Projects\cKamphuis\ddhtAI\code\' ...
          'testannot_nw\20210803_output\'];
      
% bb plot directory
mapfig = ['W:\ASG\WLR_Dataopslag\Genomica\Sustainable_breeding\'...
           '44 0000 2700 KB DDHT AI 2020\6. data\' ...
           'annotation_videos\20210803_inahulsegge\plots_bbox\'];
       
% store frames that were successfully annotated
mapsucces = ['W:\ASG\WLR_Dataopslag\Genomica\Sustainable_breeding\'...
           '44 0000 2700 KB DDHT AI 2020\6. data\' ...
           'annotation_videos\20210803_inahulsegge\annotation_success\'];
      
       
%% read annotation results
for i = 1:size(names,1)

    % filenames
    fn1 = ['ovz_annotations_' names{i} '.csv'];
    fn2 = ['annotations_' names{i} '.csv'];

    % annotator fieldname
    space = regexp(names{i},' ');
    fieldname = names{i};
    fieldname(space) = '';  % remove space
    
    % read csvs with annotation data
    opts = detectImportOptions([mapout fn1]);
    opts.Delimiter = ',';
    meta.(fieldname) = readtable([mapout fn1],opts);
    opts = detectImportOptions([mapout fn2]);
    data.(fieldname) = readtable([mapout fn2], opts);

end

% clear variables
clear opts fn1 fn2 space fieldname i

%% read frames in "mapin" to create and visualise images

% get fieldnames of data
fields_ = fieldnames(meta);

% load images
for i = 1:height(meta.(fields_{1}))
   
   % set fieldnames
   fieldname = meta.(fields_{1}).file{i};
   fieldname = fieldname(1:end-4);
   
   % load images and store figures
   images.(fieldname) = imread([mapin meta.(fields_{1}).file{i}]);
   
end

clear i

%% plot results of bb
% close all
% 
% 
% % show images based on order of first field of data
% for i = 1:height(meta.(fields_{1}))
%    % set fieldnames
%    fieldname = meta.(fields_{1}).file{i};
%    fieldname = fieldname(1:end-4);
%    
%    % show figure
%    figure('Units','centimeters','OuterPosition',[2 2 30 20]);
%    himage.(fieldname) = imshow(images.(fieldname),'InitialMagnification','fit');
%    meta.(fields_{1}).fig(i) = i;
% end
% 
% % set colors
% cols = {'r','g','b'};
% 
%  % plot bb
% for i = 1:length(fields_) 
%     for j = 1:height(data.(fields_{i}))
%         
%         % find image to plot on
%         figidx = find(contains(meta.(fields_{1}).file,...
%                           data.(fields_{i}).file{j}) == 1);
%         fig = meta.(fields_{1}).fig(figidx);
%         figure(fig);
%         
%         % plot rectangles
%         rectangle('Position',data.(fields_{i}){j,5:8},...
%                   'EdgeColor',cols{i},'LineWidth',1.2)
%     end
% end
% 
% % saveas 
% for j = 1:height(meta.(fields_{i}))
%     
%     h = figure(j);
%     title(['No. cows = ' num2str(meta.(fields_{1}).ncows(j)) ...
%            ', success = ' num2str(meta.(fields_{1}).annotation(j))])
%     saveas(h,[mapfig 'bb' meta.(fields_{1}).file{j}])
% end
% 
%     
% close all

%% list failed annotations and prepare to redo them

% copy all files to a new folder, except for those that need re-annotation
for i = 1:height(meta.(fields_{1}))
    if ~isempty(listfail)
        if ismember(meta.(fields_{1}).file(i),listfail) == 0 % if no fail
            % copy with system command
            system(['copy "' mapin '\' meta.(fields_{1}).file{i} '"'  ... 
                    ' "' mapsucces meta.(fields_{1}).file{i} '"']);  
            meta.(fields_{1}).annotation(i) = 1; % set annotation success
        else
            meta.(fields_{1}).annotation(i) = 0; % set annotation fail
        end
    else
        % copy with system command
        system(['copy "' mapin '\' meta.(fields_{1}).file{i} '"'  ... 
                ' "' mapsucces meta.(fields_{1}).file{i} '"']);  
        meta.(fields_{1}).annotation(i) = 1; % set annotation success all
    end
end

% delete all success files from mapin (don't)
% % for i = 1:height(meta.(fields_{1}))
% %     if meta.(fields_{1}).annotation(i) == 1 % if no fail
% %         % copy with system command
% %         system(['del "' mapin '\' meta.(fields_{1}).file{i} '"']);  
% %     end
% % end

% delete data and save if some files need redoing
if ~isempty(listfail)
    % delete the annotation data from data and save csv
    data.(fields_{1})(ismember(data.(fields_{1}).file,listfail),:) = [];
    writetable(data.(fields_{1}),[mapout 'annotations_' names{1} '.csv']);

    % delete the annotation metadata from meta and save csv
    meta.(fields_{1})(meta.(fields_{1}).annotation == 0,:) = [];
    try   % if figures saved first delete fig handle variable
        meta.(fields_{1}).fig = [];  % delete fig variable
    catch
    end
    writetable(meta.(fields_{1}),[mapout 'ovz_annotations_' names{1} '.csv'])
else 
    for i = 1:height(meta.(fields_{1}))
        fn_fig = meta.(fields_{1}).file{i};  % filename image
        fn_out = [fn_fig(1:end-3) 'txt'];    % filename txt
        fn_img = fn_fig(1,1:end-4);          % filename image in images
        
        ind = find(ismember(data.(fields_{1}).file,...
                           fn_fig) == 1);
                       
        % get metadata of frame
        w_img = size(images.(fn_img),2);     % image width (no col)
        h_img = size(images.(fn_img),1);     % image height (no row)
        
        % get x,y,w,h
        xmin = data.(fields_{1}).x(ind);     % first field!! of bb
        ymin = data.(fields_{1}).y(ind);     % y min of bb
        w = data.(fields_{1}).w(ind);        % width of bb
        h = data.(fields_{1}).h(ind);        % height of bb
        
        % yolo standardised
        xcenter = (xmin + w/2) ./ w_img;
        ycenter = (ymin + h/2) ./ h_img;
        w = w ./ w_img;
        h = h ./ h_img;

        % together in yoloformat                            
        yolocows = [zeros(length(ind),1), ...       % cow = label 0
                    xcenter,...    
                    ycenter,...
                    w,...
                    h];
        % write matrix
        fID = fopen([mapsucces fn_out],'w');
        fprintf(fID,'%i %f %f %f %f\r\n',yolocows');
        fclose(fID);
               
    end
end


