%% Sm4_prepare_bb_analysis
%--------------------------------------------------------------------------
% Created by Ines Adriaens, December 2nd 2021
% 
% Embedded in KB DDHT Artificial intelligence
%             
%--------------------------------------------------------------------------
% This script selects the target cow's bounding box for detecting lying and
% standing behavior based on bb shape and position.
% It reads in the location from the excel files prepared by Wijbrand
% Ouweltjes (CheckAtt_liestand_ses%%.xlsx). This contains 2 tabs:
%       - liedown: cow, nr, fb, fe, volunt, qual
%       - getup: cow, nr, fb, fe, volunt, qual
%            CheckTif_liestand.ses%%.xlsx
%       - liedown: cow, nr, x1, y1, x2, y2, x3, y3, remark
% Then, it reads in the outputs created with the tracking algorithm created
% by Ina Hulsegge, stored in ../ddhtAI/data/outp_bb
%       - .avi files with bb plotted
%       - .txt files with bb locations
% For clarification about the selection, preprocessing and editing steps of
% the videos and gold standard data, check powerpoint presentation
%                               '20210419_OverviewDDHT.pptx'
%--------------------------------------------------------------------------
% Steps:
%       1) set paths and constants
%       2) read in data + structure
%       3) select bb of target cows
%       4) check time alignation
%       5) set reference and gold standards to benchmark/develop on
%       6) save
%--------------------------------------------------------------------------

clear variables
close all
clc

%% STEP 0: set paths and constants

% folder with bb videos and txt output files (Ina)
init_.datadir = ['C:\Users\adria036\OneDrive - Wageningen University & '...
                 'Research\iAdriaens_doc\Projects\cKamphuis\ddhtAI\'...
                 'data\outp_bb\'];
        %-------------------------------------------------------------%
        % also on W drive:                                            %  
        % ['W:\ASG\WLR_Dataopslag\Genomica\Sustainable_breeding\'...  %
        %  '44 0000 2700 KB DDHT AI 2020\6. data\output_ines\'];      %
        %-------------------------------------------------------------%
             
% folder with 'gold standard' reference data (Wijbrand)
init_.docdir = ['C:\Users\adria036\OneDrive - Wageningen University & ' ...
                 'Research\iAdriaens_doc\Projects\cKamphuis\ddhtAI\'...
                 'data\'];
        %-------------------------------------------------------------%
        % also on W drive:                                            %         
        % ['W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\liestand\'];        %
        %-------------------------------------------------------------%

% filename with gs data
init_.fn1 = 'CheckAtt_liestand_ses2.xlsx';
init_.fn2 = 'CheckTif4_liestand_ses2.xlsx';
init_.fn3 = '202112_it2.xlsx';
        
%% STEP 1: read files

% list textfiles in datadir 
bb_list = ls([init_.datadir '*.txt']);

% prepare selected numbers
sel_nrs = array2table(zeros(length(bb_list),2),'VariableNames',{'nr','updown'});

% read textfiles in datadir (contains bb location and size)
for i = 1:length(bb_list)
    
    % set name
    vidname = strtrim(bb_list(i,:));
    if contains(vidname,'getup')
        field_ = ['vid_' vidname(end-7:end-4) '_up'];
    else
        field_ = ['vid_' vidname(end-7:end-4) '_do'];
    end
    
    % read bb data and calculate center of bb
    bb.(field_) = readtable([init_.datadir vidname]);
    bb.(field_)(:,7:10) = [];
    bb.(field_).Properties.VariableNames = {'frame','cow','xleft','ytop','w','h'};
    bb.(field_).x = bb.(field_).xleft + (bb.(field_).w./2);
    bb.(field_).y = bb.(field_).ytop + (bb.(field_).h./2);
    
    % add nr to list for selection + up = 1; down = 0;
    sel_nrs.nr(i) = str2double(vidname(end-7:end-4));
    sel_nrs.updown(i) = double(contains(vidname,'getup'));

end

% list video files in datadir 
vid_list = ls([init_.datadir '*.avi']);

% read video files in datadir - one frame to check
for i = 1%:length(vid_list)
    
    % set name
    vidname = strtrim(vid_list(i,:));
    if contains(vidname,'getup')
        field_ = ['vid_' vidname(end-7:end-4) '_up'];
    else
        field_ = ['vid_' vidname(end-7:end-4) '_do'];
    end

    % read vid data
    v = VideoReader([init_.datadir vidname]);
    counter = 1;
    while hasFrame(v)
        framename = ['frame_' num2str(counter)];
        vid.(field_).(framename) = readFrame(v);
        counter = counter+1;
    end

end

% read first (up) and last image (down)
for i = 1:height(vid_list)
    
    % set name
    vidname = strtrim(vid_list(i,:));
    
    % read vid data
    v = VideoReader([init_.datadir vidname]);
    counter = 1;
    if contains(vidname,'getup')
        field_ = ['vid_' vidname(end-7:end-4) '_up'];
        while hasFrame(v) && counter < 3
            image.(field_) = readFrame(v);
            counter = counter+1;
        end
        image.(field_) = readFrame(v);
    else
        field_ = ['vid_' vidname(end-7:end-4) '_do'];
        while hasFrame(v)
            image.(field_) = readFrame(v);
        end
    end
end


% read gold standard/reference data
gs.liedown = readtable([init_.docdir init_.fn1],'Sheet','liedown');
gs.getup = readtable([init_.docdir init_.fn1],'Sheet','getup');
opts = detectImportOptions([init_.docdir init_.fn2],'Sheet','liedown');
opts.SelectedVariableNames = {'cow','nr','cam','x1',...
                              'y1','x2','y2','x3','y3','remark'};
gs.general = readtable([init_.docdir init_.fn2],opts,'Sheet','liedown');

% select the gs data of the selected videos
gs.general = innerjoin(gs.general,sel_nrs);
gs.liedown = innerjoin(gs.liedown,sel_nrs(sel_nrs.updown==0,:));
gs.getup = innerjoin(gs.getup,sel_nrs(sel_nrs.updown==1,:));

% delete data where quality is not guaranteed or where remark indicates
% false bouts (for now, manually selected numbers)
%    iter1     % gs.exclude = [142,0;...
        %               266,0;...
        %               364,0;...
        %               350,1;...
        %               560,1;...
        %               751,1];
gs.exclude = gs.liedown{gs.liedown.qual == 0,[2 7]};
temp = gs.getup{gs.getup.qual ~= 1,[2 7]};
gs.exclude = [gs.exclude;temp];
temp = gs.general{find(~strcmp('',gs.general.remark)),[2 11]};
gs.exclude = sortrows(unique([gs.exclude;temp],'rows'));


% remaining: select
gs.general(ismember(gs.general.nr,gs.exclude),:)=[];
gs.liedown(ismember(gs.liedown.nr,gs.exclude),:)=[];
gs.getup(ismember(gs.getup.nr,gs.exclude),:)=[];
sel_nrs(ismember(sel_nrs.nr,gs.exclude),:)=[];
for i = 1:size(gs.exclude,1)
    
    if gs.exclude(i,2) == 1
        % set name for getup
        field_ = ['vid_0' num2str(gs.exclude(i)) '_up'];
        if strlength(field_) ~= 11
            field_ = ['vid_00' num2str(gs.exclude(i)) '_up'];
        end
    else
        % set name for setdown
        field_ = ['vid_0' num2str(gs.exclude(i)) '_do'];
        if strlength(field_) ~= 11
            field_ = ['vid_00' num2str(gs.exclude(i)) '_do'];
        end
    end
    
    % remove fields
    bb = rmfield(bb,field_);      % remove field from bb
%     vid = rmfield(vid,field_);    % remove field from vid
    
end



% clear variables
clear i ans bb_list counter frame framename opts v vid_list vidname field_

%% STEP 2: select animal that does lying/getup in detected bb
% based on the combined coordinates of gs.general (x,y) and the bb
% locations

% center of withers, tail, navel
gs.general.cowcenter_x = mean([gs.general.x1, ...
                               gs.general.x2, ...
                               gs.general.x3],2);
gs.general.cowcenter_y = mean([gs.general.y1, ...
                               gs.general.y2, ...
                               gs.general.y3],2);
                           
%--------------------------------------------------------------------------
% the coordinates of CheckTif4_liedown_ses%.xlsx contain for each video the
% coordinates of the animal when the target cow is lying down. Distance can
% only be used in that case, as in other situations the cow might be
% walking and another cow can be closest to the target coordinate.
%       In the down videos- use final 50 frames
%       In the up videos- use first 50 frames
% IF no change of identity with first distance criterion- no problem, as
% the target cow remains closest of all cows to the initial point
% IF a cow identity change happens, it can be because:
%       - the cow moves and another cow is closest
%       - yolo and deepsort have not tracked the original cow
%       - identity switch with another cow (like in 0134_up)
%    see how far the 'right cow' as based on minimal distance first 50 or
%    last 50 frames are tracked
%--------------------------------------------------------------------------

                           
                           
% summary bb locations average per video
field_ = fieldnames(bb);
for i = 1:length(field_)
    
    % nr
    nr = str2double(regexp(field_{i},'\d*','Match'));
    % updown
    ud = double(contains(field_{i},'up')); % 1 if getup
    
    % calculate euclidian distance of each bb to cowcenter
        % for now, assume that it gives the center of the bb in bb (x,y)
    idx = find(gs.general.nr == nr); % find correct number to pair with
    bb.(field_{i}).eucldist = sqrt(...
                (bb.(field_{i}).x-gs.general.cowcenter_x(idx(1))).^2 + ...
                (bb.(field_{i}).y-gs.general.cowcenter_y(idx(1))).^2); 
    
    % per frame, select the bb for which the difference with the cowcenter
    % is smallest, AND, in case that the cow is not detected or there is an
    % identity shift (because no bb or deepsort fails), the distances does
    % not shift too much 
    
    % per frame, select bb with lowest euclidean difference
    frames = unique(bb.(field_{i}).frame);
    for j = 1:length(frames)
        bb.(field_{i}).ismindist(bb.(field_{i}).frame == frames(j) & ...
                                 bb.(field_{i}).eucldist == ...
                                 min(bb.(field_{i}).eucldist(...
                                 bb.(field_{i}).frame == frames(j)))) = 1;
    end
    
    % select bb of the mincenterdist cows
    bb_sel.(field_{i}) = bb.(field_{i})(bb.(field_{i}).ismindist == 1, :);
    
    % if more than one cow/track -- decide which bb are correct
    if length(unique(bb_sel.(field_{i}).cow)) ~= 1
        disp([field_{i} ', cow selection needed'])
    end
        
    % if standing up - detect whether deepsort kept track of initial cow
    % detected
    if contains(field_{i},'up')         % select 50 first frames if up
        cowid = round(mean(bb_sel.(field_{i}).cow(1:50)));        
    else    % select 50 last frames if down
        cowid = round(mean(bb_sel.(field_{i}).cow(end-49:end)));
    end
    
    % if there is data available throughout the video for this cow, select
    % these data and determine % missing, exclude when too much uncertainty
    % and too low quality of the detection
    ind = find(bb.(field_{i}).cow == cowid);
    if length(ind) > 0.9*max(bb.(field_{i}).frame)
        bb_sel.(field_{i}) = bb.(field_{i})(ind,:);
    else
        disp([field_{i} ', excluded'])
        bb_sel = rmfield(bb_sel, field_{i});
    end
    
    % add selected cow id for checkup to sel_nrs
    sel_nrs.cowid(sel_nrs.nr == nr & sel_nrs.updown == ud) = cowid;
    
    
end

%%%%%%%% load checked videos
%       this file contains manual id and checkup information
%       headers: vid, nr, cowid1, cowid2, exclude
%       sheets: it2_getup, it2_liedown
gs.check_up = readtable([init_.datadir,init_.fn3],'Sheet','it2_getup');
gs.check_do = readtable([init_.datadir,init_.fn3],'Sheet','it2_liedown');
sel_nrs = innerjoin(sel_nrs,...
      [gs.check_up(:,[2 3 5 6]);gs.check_do(:,[2 3 5 6])],...
        'Keys',{'nr','updown'});
sel_nrs = sortrows(sel_nrs,'updown','descend');

% manual check of quality and cowid, added to sel_nrs
sel_nrs.Properties.VariableNames{4} = 'cowidcheck';
sel_nrs.Properties.VariableNames{5} = 'quality';

% remove fields if not already done
ind = find(sel_nrs.quality == 0);
for i = 1:length(ind)
    
    
    % name
    if sel_nrs.updown(ind(i)) == 1
        % set name for getup
        field_ = ['vid_0' num2str(sel_nrs.nr(ind(i))) '_up'];
        if strlength(field_) ~= 11
            field_ = ['vid_00' num2str(gs.exclude(i)) '_up'];
        end
        
        % remove row from gs.getup
        
    else
        % set name for setdown
        field_ = ['vid_0' num2str(sel_nrs.nr(ind(i))) '_do'];
        if strlength(field_) ~= 11
            field_ = ['vid_00' num2str(gs.exclude(i)) '_do'];
        end
    end
    
    % remove fields from bb_sel
    try
        bb_sel = rmfield(bb_sel,field_);
    catch
        % do nothing
    end
end
sel_nrs(sel_nrs.quality == 0,:) = [];

% clear workspace 
clear cowid field_ frames i idx ind j nr ud 


%% STEP 3: Save workspace

save([init_.datadir 'D0_bb_selection_it2.mat'])



