# -*- coding: utf-8 -*-
"""
Created on Wed Jun 23 18:02:27 2021

@author: adria036

This script extracts individual frames from videos with the purpose to 
    calculate background, and summarize statistical properties
    - can save frames as .jpg separately (evaluation purposes);
    - stores the read/required data in a data array
    - calculates the statistical properties of the selected frames
    
"""

#%% Import packages and modules

import os
import cv2
import numpy as np
from scipy import stats
import pandas as pd
from matplotlib import pyplot as plt

#%% Set file paths, filenames and constants

# source directory
src = r'C:/Users/adria036/OneDrive - Wageningen University & ' \
     + 'Research/iAdriaens_doc/Projects/cKamphuis/ddhtAI/data/vids/'

# filenames in list with conditions as specified below
fn = [f for f in os.listdir(src) if os.path.isfile(src+f) \
        and ".mp4" in f \
        and "ch01_" in f]

# number of frames in fn
noframes = np.zeros((len(fn),1),dtype=int)
for i in range(0,len(fn)):
    vid = cv2.VideoCapture(src+fn[i])
    noframes[i] = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
    vid.release()

# equally spaced frame reading step (all frames = 1), x% of nframes
fstep = 2000 #np.uint8(noframes.mean()//5000)



#%% Read videos and store specific frames in an array

# prepare video reads
check = True                            # check = there's something to capture
frames = []                             # empty var frames to fill in

# capture video + read frame
for i in range(0,len(fn)):
    count = 0                           # count = no. of frames read
    cap = cv2.VideoCapture(src+fn[i])   # capture video
    check, vid = cap.read()             # first frame data
    frames.append(vid)                  # append video data to 'frames'
    count += 1                          # increase count with one

    # read while cap is not empty and store in array
    while check:
        # cv2.imwrite("frame%d.jpg" % count, vid)     # to write frames as jpg
        check,vid = cap.read()          # read next frame
        count += 1                      # increase count with one
        if count % fstep == 0:          # use division for read condition
            frames.append(vid)          # append to frames
            print("i = " + str(i))

    # if cap.read check is false - release
    cap.release()
    #frames.pop()                        # delete last element = nonetype


cv2.imshow("window",frames[0])          # show the first frame
k = cv2.waitKey(0) & 0xFF               # avoid crashing of kernel
if k != 0:                              # wait for ESC key to exit
    cv2.destroyAllWindows()

#%% Statistical background properties of frames summary + visualisation
 
# mean frame (if enough data and camera position unchanged = background)
meanFrame = np.mean(frames, axis = 0)       # dim 0 = over all frames
meanFrame = np.uint8(meanFrame)             # float to int for displaying

# mode of frame (most frequent value)
modeFrame = stats.mode(frames, axis = 0)    # mode of images
modeFrame = np.asarray(modeFrame[0])        # as array     
modeFrame = modeFrame[0]                    # select first dimension

# visualisation of average frame values
cv2.imshow('meanimage', meanFrame)          # show the result of meanFrame
# cv2.imshow('modeimage', modeFrame)        # show the result of modeFrame
cv2.waitKey(0)   & 0xFF                     # avoid crashing of kernel
if k != 0:                                  # wait for ESC key to exit
    cv2.destroyAllWindows()


#%% Image properties summary in dataframe -- per FRAME

# construct feature vector of each frame
stats = np.zeros((len(frames),6), dtype = float)      # prepare stats
for i in range(0,len(frames)):
    #raw = frames[i].flatten()  # this is just a flattend version of the image
    bgrmean, bgrstd = cv2.meanStdDev(frames[i])  # mean and std
    stats[i,:] = np.reshape([bgrmean, bgrstd],(1,6))  # fill stats

# convert to pandas data frame
stats = pd.DataFrame(data=stats,
                     columns = ['bmean','gmean','rmean','bstd','gstd','rstd'])
    
# calculate the histograms of pixel values per frame and store in 'framehists'
for i in range(0,len(frames)):
    # no of bins per histogram (= black/white, R, G, B)
    nbins = 50                              # no of bins
    colname = 'frame_' + str(i)
    # calculate histogram of gray scale image
    grayimg = cv2.cvtColor(frames[i],cv2.COLOR_BGR2GRAY)  # gray image
    histgray = cv2.calcHist([grayimg],      # input = gray image
                            [0],            # channel = single 
                            None,           # mask = none
                            [nbins],          # no of bins = 256 (gray values)
                            [0,256])        # range of pixel values
    histgray = np.array(histgray)
        
    
    chans = cv2.split(frames[i])            # split frame i in its bgr chans
    colors = ("b", "g", "r")
    plt.figure(figsize=[12,8])
    plt.title("'Flattened' Color Histogram")
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    features = []
    for (chan, color) in zip(chans, colors):
        hist = cv2.calcHist([chan], [0], None, [50], [0, 256])
        features.extend(hist)
        plt.plot(hist, color = color)
        plt.xlim([0, 50])
    arr = np.array(features)               # 
    newarr = np.vstack((histgray,arr))     # to numpy array
    
    
    if i == 0:
        framehists = pd.DataFrame(data = newarr,
                                  columns = [colname])
    else:
        framehists[colname] = newarr

framehists = framehists.astype(int)         # as integers


cv2.imshow('test', frames[i])          # show the result of meanFrame
# cv2.imshow('modeimage', modeFrame)        # show the result of modeFrame
cv2.waitKey(0)                              # avoid crashing of kernel
if k != 0:                                 # wait for ESC key to exit
    cv2.destroyAllWindows()
    
    
# Euclidian distance between columns
for i in range(0,len(frames)):
    for j in range(0,len(frames)):
        distGrey = np.linalg.norm(A - B)
#        distColor = np.
# sampling > take 2 most distant frames > take most distant frames etx
    
    
    # visualisation of samples?

#plt.figure(figsize=[12,8])
#plt.title("Grayscale Histogram")
#plt.xlabel("Bins")
#plt.ylabel("# of Pixels")
#plt.plot(histgray)
#plt.xlim([0, 256])
#
#plt.figure(figsize=[12,8])
#plt.title("Grayscale Histogram")
#plt.xlabel("Bins")
#plt.ylabel("# of Pixels")
#plt.plot(histgray)
#plt.xlim([0, 256])

cv2.imshow('grayimage', grayimg) 
k = cv2.waitKey(0) & 0xFF                   # avoid crashing of kernel
if k == 27:                                 # wait for ESC key to exit
    cv2.destroyAllWindows()


hist,bins = np.histogram(frames[i],256,[0,256])

# combine results in pandas dataframe
imgproperties = pd.DataFrame(data=None, index=None, dtype=None, copy=False)


# RGB mean and standard deviation 

b = frames[i]

cv2.imshow('blueimage', b)           # show the result of meanFrame
k = cv2.waitKey(0) & 0xFF                   # avoid crashing of kernel
if k == 27:                                 # wait for ESC key to exit
    cv2.destroyAllWindows()
    
    


#%% Properties of all (selected) images/frames in video - per pixel summaries




