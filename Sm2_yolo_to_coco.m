%% Sm1_yolo_to_coco.m
% This script combines .txt files with yolo annotations and stores them 
% in json coco formatn for use with the openMM toolbox for deep learning
% information on the format was gathered from Tomas Izquierdo and the 
% https://mmdetection.readthedocs.io/en/latest/tutorials/customize_dataset.html


%% set filepaths and constants

% output file
f_out = 'coco_out.txt';         

% input folder
filedir = ['W:\ASG\WLR_Dataopslag\Genomica\Sustainable_breeding\'...
           '44 0000 2700 KB DDHT AI 2020\6. data\' ...
           'annotation_videos\20210803_inesadriaens\annotation_success\'];

% file list of .txt files
files = ls([filedir '*.txt']);

%% set up structure base

s.licenses.name = "Ines Adriaens";
s.licenses.id = "adria036";
s.licenses.url = "";
s.info.contributor = "";
s.info.date_created = "04/08/2021";
s.info.description = "";
s.info.url = "";
s.info.version = "";
s.info.year = "";
s.categories.id = 1;
s.categories.name = "cow";
s.categories.supercategory = "";

% add annotations
for i = 1:length(files)
    filename = strtrim(files(i,:));
    id = i;
end

jsonencode(s)




%% initialize output file







