% program to determine bounding boxes around cows for further processing
clear;

% --- provide input and output folders ---
%mapin =  [ cd '\input']; % --- folder where the frames to be annotated are stored (currently: working folder!) ---
mapin = 'W:\ASG\WLR_Dataopslag\Genomica\Sustainable_breeding\44 0000 2700 KB DDHT AI 2020\6. data\annotation_videos\20210803_inesadriaens';
mapout = [ cd '\20210803_output']; % --- folder where output is stored (currently: working folder!) ---
% --- ---

% --- store folder where program is stored ---
mapstart = cd;
% --- ---

% --- tell who is annotating ---
disp('Who is going to annotate?');
T = readtable('annotators.xlsx','ReadVariableNames',true); names = table2cell(T); clear T;
for i=1:size(names,1)
    disp(['    ' num2str(i) ' = ' cell2mat(names(i))]);
end
disp('If you are not in the list: first add your name to annotators.xlsx (on the bottom of the list)!');
iannot = input('Give the right number : ');
if (iannot < 1 || iannot > size(names,1)); error('Add your name to annotators.xlsx'); end
annot = cell2mat(names(iannot));
% --- ---

% --- store name of utputfiles in variables f_out1 and f_out2 ---
f_out1 = ['ovz_annotations_' annot '.csv'];
f_out2 = ['annotations_' annot '.csv'];
% --- ---

% --- get date to keep track of things ---
idat = str2double(datestr(date,'yyyymmdd'));
% --- ---

% --- make listing of jpg-files in the given folder ---
[sts,cmd] = dos(['dir/b/o "' mapin '\"*.jpg -> jpgfiles.lst']);
% --- ---

% --- read the filenames in memory (variable list1) ---
fID = fopen('jpgfiles.lst'); idoor = 1; nf = 0;
while (idoor==1)
    tline = fgets(fID); il = size(tline,2)-2; if (il < 5); idoor=0; break; end
    nf = nf + 1;
    list1(nf,:)=cellstr(tline(1:il));
end
fclose('all'); dos('del jpgfiles.lst');
% --- ---

% --- read or if needed make check-file to start at the right event ---
cd(mapout);
if (exist(f_out1,'file') ~= 2)
    list2 = zeros(nf,5); for i = 1:nf; list2(i,:) = NaN; end
    Tann1 = table('Size',[0 6],'VariableTypes',{'cell','double','double','double','double','double'});
    Tann1.Properties.VariableNames = {'file','annotator','date','ncows','quality','annotation'};
    Tann2 = table('Size',[0,12],'VariableTypes',{'cell','double','double','double','double','double','double','double','double','double','double','double'});
    Tann2.Properties.VariableNames = {'file','annotator','date','cownr','x','y','w','h','shoulder_x','shoulder_y','tail_x','tail_y'};
else
    opts1 = detectImportOptions(f_out1);
    opts1.VariableNames = {'file','annotator','date','ncows','quality','annotation'};
    opts1 = setvartype(opts1, {'annotator','date','ncows','quality','annotation'},'double');
    Tann1 = readtable(f_out1,opts1);    
    opts2 = detectImportOptions(f_out2);
    opts2.VariableNames = {'file','annotator','date','cownr','x','y','w','h','shoulder_x','shoulder_y','tail_x','tail_y'};
    opts2 = setvartype(opts2, {'annotator','date','cownr','x','y','w','h','shoulder_x','shoulder_y','tail_x','tail_y'},'double');
    Tann2 = readtable(f_out2,opts2);
    % --- check if the two files are consistent: 
    cows = table2array(Tann1(:,4));
    files1 = table2cell(Tann1(:,1));
    files2 = table2cell(Tann2(:,1));
    j = 0; nk = 0;
    for i = 1:size(cows,1)
        if (~isnan(cows(i)))
            nk = nk+ cows(i);
            name1 = cell2mat(files1(i,:));
            for ii = 1:cows(i)
                j = j + 1;
                name2 = cell2mat(files2(j,:));
                if (~strcmp(name1,name2)); error([' files ' f_out1 ' and ' f_out2 ' are inconsistent!']); end
            end
        end
    end
    % --- remove redundant records from Tann2 (if existing: hen lines in ovz_annotation are deleted this can occur) --- 
    for i=size(files2,1):-1:1
        name2 = cell2mat(files2(i,:)); ifnd = 0;
        for j=1:size(files1,1)
             name1 = cell2mat(files1(j,:));
             if (strcmp(name1,name2)); ifnd = 1; break; end
        end
        if (ifnd == 0); Tann2(i,:) = []; end
    end
    % --- now skip frames already annotated ---
    [list1,nf,list2,Tann1]=chklist(list1,nf,files1,Tann1,cows);
    % --- ---
end
% --- ---

% --- initialise list3 and list4 ---
list3 = table2cell(Tann2(:,1)); list3(:,:) = [];
list4 = table2array(Tann2(:,2:12)); list4(:,:) = [];
% --- ---

% --- now annotate the frames ---
nannot = 0; tcow = 0; nshow = 0;
for i=1:nf
    if (~isnan(list2(i,1))); continue; end % annotation is already completed!
    pict = [mapin '\' cell2mat(list1(i))];
    disp(num2str(i))
    img = imread(pict); [my,mx,~]=size(img);
    nshow = nshow + 1;
    if (nshow > 1)
        figure('Position',pos);
    end
    clf; imshow(img,'InitialMagnification','fit'); hold on;
    if (nshow == 1)
        title('put the figure frame in the desired position on the screen, press the spacebar when ready','Fontsize',8,'Interpreter','none');
        ibut = 0;
        while (ibut ~= 32) 
            [~,~,ibut] = ginput(1);
        end
    end
    % --- first judge quality of the frame ---
    atit1='give an estimate of the quality of the image (mouseclick = OK, spacebar = not OK)';
    title(atit1,'Fontsize',14,'Interpreter','none');
    ibut = 0;
    while (ibut ~= 32 && ibut ~= 1 && ibut ~= 2 && ibut ~= 3) 
        [~,~,ibut] = ginput(1);
    end
    if (ibut == 32); qual = 0; else; qual = 1; end
    % --- find the cows approximately ---
    atit1='For each cow that is visible click on approximate position (press the spacebar if you are finished)';
    title(atit1,'Fontsize',14,'Interpreter','none');
    text(mx/2,my+50,'Enter "Esc"  to stop','Color','red','FontSize',16,'HorizontalAlignment','center');
    idoor = 1; ncow = 0; clear arcow;
    while (idoor == 1)
        [x,y,ibut] = ginput(1);
        if (ibut == 27 && ncow == 0); break; end
        if (ibut == 32); idoor = 0; break; end
        if (ibut ~= 1); disp(' give a mouseclick instead of input via the keyboard!'); continue; end
        ncow = ncow + 1; tcow = tcow +1;
        if (ncow == 1)
            clf;
            imshow(img,'InitialMagnification','fit'); hold on;
            title(atit1,'Fontsize',14,'Interpreter','none');
        end
        list3(tcow,1) = list1(i);
        list4(tcow,1:5) = [iannot idat ncow x y];
        plot(x,y,'gx','MarkerSize',10,'LineWidth',2);        
    end
    if (ibut == 27 && ncow == 0); close(gcf); break; end
    list2(i,1) = iannot;
    list2(i,2) = idat;
    list2(i,3) = ncow;
    list2(i,4) = qual;
    f = gcf; pos = f.Position; % for use with next cow and so on
    % --- now set the bounding boxes ---
    for j=1:ncow
        if (j > 1)
            figure('Position',pos);
        end
        f = gcf; clf; imshow(img,'InitialMagnification','fit'); hold on;
        plot(list4(tcow-ncow+j,4),list4(tcow-ncow+j,5),'gx','MarkerSize',14,'LineWidth',2); 
        atit2=['set YOLO bounding box for cow ' num2str(j) ' of ' num2str(ncow) ' (adjust box if necessary and press spacebar when ready)'];
        title(atit2,'Fontsize',12,'Interpreter','none');
        [roi0]=initroi(list4,tcow,ncow,j,mx,my);
        [roi] = getroi(roi0);
        list4(tcow-ncow+j,4:7) = roi;
        clear h;
        atit2=['click on shoulder of cow ' num2str(j) ' of ' num2str(ncow)];
        title(atit2,'Fontsize',12,'Interpreter','none');
        x1=[]; ibut = 0;
        while (isempty(x1) || ibut < 1 || ibut > 3)
            [x1,y1,ibut] = ginput(1);
        end
        plot(x1,y1,'gx','MarkerSize',10,'LineWidth',2);        
        atit2=['click on top of tail of cow ' num2str(j) ' of ' num2str(ncow)];
        title(atit2,'Fontsize',12,'Interpreter','none');
        x2=[]; ibut = 0;
        while (isempty(x2) || ibut < 1 || ibut > 3)
            [x2,y2,ibut] = ginput(1);
        end
        plot(x2,y2,'gx','MarkerSize',10,'LineWidth',2); 
        list4(tcow-ncow+j,8:11) = [x1 y1 x2 y2];
        if (j < ncow); close(f); end
    end
    % --- finally judge if the annotation went OK (0 = no, 1 = yes) ---
    atit1='Is the annotation of this frame OK? (mouseclick = yes, spacebar = no)';
    title(atit1,'Fontsize',14,'Interpreter','none');
    ibut = 0;
    while (ibut ~= 32 && ibut ~= 1 && ibut ~= 2 && ibut ~= 3) 
        [~,~,ibut] = ginput(1);
    end
    if (ibut == 32); list2(i,5) = 0; else; list2(i,5) = 1; end
    if (nannot == 10)
        makeoutput(list1,list2,Tann1,list3,list4,Tann2,f_out1,f_out2);
        nannot = 0;
    end
    close(f);
    nannot = nannot + 1;
end
% --- write the output to files ---
if (nannot > 0); makeoutput(list1,list2,Tann1,list3,list4,Tann2,f_out1,f_out2); end
% --- ---

% --- go back to original folder ---
cd(mapstart)
% --- ---

disp('set_boundingboxes is finished');
function [list1,nf,list2,Tann1]=chklist(list1,nf,files1,Tann1,cows)
arh=zeros(nf,1); for i = 1:nf; arh(i,1) = NaN; end
for i=1:nf
    name1 = cell2mat(list1(i,:));
    for j=1:size(files1,1)
        name2 = cell2mat(files1(j,:));
        if (strcmp(name1,name2)); arh(i)=cows(j); break; end
    end
end
for i=nf:-1:1
    if (arh(i) >= 0); list1(i,:)=[]; end
end
nf=size(list1,1);
list2 = zeros(nf,5); for i = 1:nf; list2(i,:) = NaN; end
for i=size(cows,1):-1:1
     if (isnan(cows(i))); Tann1(i,:) = []; end
end
return
end   
function makeoutput(list1,list2,Tann1,list3,list4,Tann2,f_out1,f_out2)
% --- for safety make a copy of existing output files ---
if (exist(f_out1,'file') == 2)
    f_out1n = ['old_' f_out1]; 
    cmd = ['copy "' f_out1 '" "' f_out1n '"'];
    dos(cmd);
    f_out2n = ['old_' f_out2]; 
    cmd = ['copy "' f_out2 '" "' f_out2n '"'];
    dos(cmd);
end
% --- ---

% --- put variables into tables and merge with old output ---
T0 = cell2table(list1); T0.Properties.VariableNames = {'file'};
T1 = array2table(list2); T1.Properties.VariableNames = {'annotator','date','ncows','quality','annotation'};
TH = [T0 T1];
T = [Tann1;TH];
writetable(T,f_out1);
T2 = cell2table(list3); T2.Properties.VariableNames = {'file'};
T3 = array2table(list4); T3.Properties.VariableNames = {'annotator','date','cownr','x','y','w','h','shoulder_x','shoulder_y','tail_x','tail_y'};
TH = [T2 T3];
T = [Tann2;TH];
writetable(T,f_out2); clear T*;
% --- ---

return
end
function [roi0]=initroi(list4,tcow,ncow,j,mx,my)
roi0(1) = round(list4(tcow-ncow+j,4) - 250,0);
roi0(2) = round(list4(tcow-ncow+j,5) - 250,0);
if (roi0(1) < 1); roi0(1) = 1;
elseif (roi0(1) > mx - 500); roi0(1) = mx-500; end
if (roi0(2) < 1); roi0(2) = 1;
elseif (roi0(2) > my - 500); roi0(2) = my-500; end
roi0(3) = 500;
roi0(4) = 500;
return
end
function [roi] = getroi(roi0)
h = drawrectangle('Position',[roi0(1),roi0(2),roi0(3),roi0(4)],'Color','g','FaceAlpha',0.1);
ibut = 0;
while ibut ~= 32
    [~,~,ibut] = ginput(1);
end
roi = h.Position;
return;
end
