%% Sm5_bb_liedown_getup
%--------------------------------------------------------------------------
% Created by Ines Adriaens, December 2nd 2021
% 
% Embedded in KB DDHT Artificial intelligence
%             
%--------------------------------------------------------------------------

% preprocessing bb -  smooth
% charactize / calculate bb features -- time seties
% train for detection of changes
% check with gold standard

clear variables 
close all
clc

%% STEP 0: set filepaths and constants, load data

% data directory
init_.datadir = ['C:\Users\adria036\OneDrive - Wageningen University & '...
                 'Research\iAdriaens_doc\Projects\cKamphuis\ddhtAI\'...
                 'data\outp_bb\'];
             
% read data
% load([init_.datadir 'D0_bb_selection.mat'])  % iteration 1
load([init_.datadir 'D0_bb_selection_it2.mat'])

% results directory
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen University & '...
                 'Research\iAdriaens_doc\Projects\cKamphuis\ddhtAI\'...
                 'data\results_bb\'];
             
% % iteration 1 replace nr 140= cow 2 ipv 3
% bb_sel.vid_0140_up = bb.vid_0140_up(bb.vid_0140_up.cow == 2,:);
% 

%% STEP 1: smooth bb
% smooth bb to avoid jumps and noise

% fieldnames
fields_ = fieldnames(bb_sel);

% plot time series, test smoothing
close all
for i = 1:length(fields_)
    % prepare plot
    f = figure('Units','centimeters','OuterPosition',[52 -2 40 20]);
    subplot(2,2,1);hold on; box on; xlabel('frame'); ylabel('x')
    subplot(2,2,2);hold on; box on; xlabel('frame'); ylabel('y')
    subplot(2,2,3);hold on; box on; xlabel('frame'); ylabel('w')
    subplot(2,2,4);hold on; box on; xlabel('frame'); ylabel('h')
    for j = 1:4; subplot(2,2,j); title(fields_{i}, 'Interpreter','none'); end
    
    % set ylimits
    subplot(2,2,1);ylim([min(bb_sel.(fields_{i}).x)-10 max(bb_sel.(fields_{i}).x)+10])
    subplot(2,2,2);ylim([min(bb_sel.(fields_{i}).y)-10 max(bb_sel.(fields_{i}).y)+10])
    subplot(2,2,3);ylim([min(bb_sel.(fields_{i}).w)-10 max(bb_sel.(fields_{i}).w)+10])
    subplot(2,2,4);ylim([min(bb_sel.(fields_{i}).h)-10 max(bb_sel.(fields_{i}).h)+10])
    
    
    % plot x,y,w,h
    subplot(2,2,1);plot(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).x,...
                        'o-','LineWidth',1.2,'MarkerSize',2,'Color',[0 0 102]./255)
    subplot(2,2,2);plot(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).y,...
                        'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 102]./255)
    subplot(2,2,3);plot(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).w,...
                        'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 102]./255)
    subplot(2,2,4);plot(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).h,...
                        'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 102]./255)
    
    % smooth over the entire frame (fit 3th and 4th order polynomial)
    x = bb_sel.(fields_{i}).frame;
    y = bb_sel.(fields_{i}).x; x5 = smooth(y,25,'sgolay',3); bb_sel.(fields_{i}).x5 = x5;
    y = bb_sel.(fields_{i}).y; y5 = smooth(y,25,'sgolay',3); bb_sel.(fields_{i}).y5 = y5;
    y = bb_sel.(fields_{i}).w; w5 = smooth(y,25,'sgolay',3); bb_sel.(fields_{i}).w5 = w5;
    y = bb_sel.(fields_{i}).h; h5 = smooth(y,25,'sgolay',3); bb_sel.(fields_{i}).h5 = h5;
    
    % plot smoothed
    subplot(2,2,1);plot(x,x5,'r-','LineWidth',1.7);
    subplot(2,2,2);plot(x,y5,'r-','LineWidth',1.7);
    subplot(2,2,3);plot(x,w5,'r-','LineWidth',1.7);
    subplot(2,2,4);plot(x,h5,'r-','LineWidth',1.7);

    % plot GS behavior end and begin
    nr = str2double(regexp(fields_{i},'\d*','Match'));
    if contains(fields_{i},'up')
        framestart = gs.getup.fb(gs.getup.nr == nr);
        frameend = gs.getup.fe(gs.getup.nr == nr);
    else
        framestart = gs.liedown.fb(gs.liedown.nr == nr);
        frameend = gs.liedown.fe(gs.liedown.nr == nr);
    end
    
    % plot GS behavior end and begin
    for j = 1:4
        subplot(2,2,j); %yyaxis left
        plot([framestart framestart],[0 2000],'r--','LineWidth',1.2)
        plot([frameend frameend],[0 2000],'r--','LineWidth',1.2)
    end
    
    % save figures
    saveas(f,[init_.resdir 'f_smooth_' fields_{i} '.tif' ]);
    close all
end



%% STEP 2: calculate bb characteristics 
%   location of center
%   surface area
%   ratio width and lenght
%   derivatives?

% plot time series, test smoothing
close all
for i = 1:length(fields_)
    
    % x and y are center of bb -- distance of x,y from center of frame
    % frames are 1280*720; center is [640,360]; use smoothed!
    bb_sel.(fields_{i}).centerdist = ...
        sqrt((bb_sel.(fields_{i}).x5 - 640).^2 + ...
             (bb_sel.(fields_{i}).y5 - 360).^2);
        
    % surface area based on smoothed
    bb_sel.(fields_{i}).area = ...
        bb_sel.(fields_{i}).w5 .* bb_sel.(fields_{i}).h5;
    
    % ratio width and length
    bb_sel.(fields_{i}).whratio = ...
        bb_sel.(fields_{i}).w5 ./ bb_sel.(fields_{i}).h5;
    
    % circumference
    bb_sel.(fields_{i}).circum = ...
        bb_sel.(fields_{i}).w5*2+bb_sel.(fields_{i}).h5*2;
    
    
    % prepare plot
    f = figure('Units','centimeters','OuterPosition',[50 -2 50 15]);
    subplot(1,4,1); hold on; box on; xlabel('frame'); ylabel('centerdist')
    subplot(1,4,2); hold on; box on; xlabel('frame'); ylabel('area')
    subplot(1,4,3); hold on; box on; xlabel('frame'); ylabel('whratio')
    subplot(1,4,4); hold on; box on; xlabel('frame'); ylabel('circumference')

    % add title
    for j = 1:4; subplot(1,4,j); title(fields_{i}, 'Interpreter','none'); end

    % plot GS behavior end and begin
    nr = str2double(regexp(fields_{i},'\d*','Match'));
    if contains(fields_{i},'up')
        framestart = gs.getup.fb(gs.getup.nr == nr);
        frameend = gs.getup.fe(gs.getup.nr == nr);
    else
        framestart = gs.liedown.fb(gs.liedown.nr == nr);
        frameend = gs.liedown.fe(gs.liedown.nr == nr);
    end
    
    % plot GS behavior end and begin
    for j = 1:4
        subplot(1,4,j); %yyaxis left
        plot([framestart framestart],[0 50000],'r--','LineWidth',1.2)
        plot([frameend frameend],[0 50000],'r--','LineWidth',1.2)
    end
    
    % plot bb characteristics
    x = bb_sel.(fields_{i}).frame;
    subplot(1,4,1);plot(x,bb_sel.(fields_{i}).centerdist,...
             'o-','LineWidth',1.2,'MarkerSize',2,'Color',[0 0 102]./255)
    subplot(1,4,2);plot(x,bb_sel.(fields_{i}).area,...
             'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 102]./255)
    subplot(1,4,3);plot(x,bb_sel.(fields_{i}).whratio,...
             'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 102]./255)
    subplot(1,4,4);plot(x,bb_sel.(fields_{i}).circum,...
             'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 102]./255)

    % set ylimits
    subplot(1,4,1);ylim([min(bb_sel.(fields_{i}).centerdist)-10 ...
                         max(bb_sel.(fields_{i}).centerdist)+10])
    subplot(1,4,2);ylim([min(bb_sel.(fields_{i}).area)-10 ...
                         max(bb_sel.(fields_{i}).area)+10])
    subplot(1,4,3);ylim([min(bb_sel.(fields_{i}).whratio)-0.05 ...
                         max(bb_sel.(fields_{i}).whratio)+0.05])
    subplot(1,4,4);ylim([min(bb_sel.(fields_{i}).circum)-0.05 ...
                         max(bb_sel.(fields_{i}).circum)+0.05])
                     
    % save
    saveas(f,[init_.resdir 'f_smooth_' fields_{i} 'bbprop.tif' ]);
    
end

% standardize the bb characteristics - x5,y5,w5,h5,centerdist,area,whratio
for i = 1:length(fields_)
    
    % min-max standardisation - (val-min)/(max-min)
    bb_sel.(fields_{i}).x5_st = ...
        (bb_sel.(fields_{i}).x5 - min(bb_sel.(fields_{i}).x5)) ./ ...
        (max(bb_sel.(fields_{i}).x5) - min(bb_sel.(fields_{i}).x5));
    bb_sel.(fields_{i}).y5_st = ...
        (bb_sel.(fields_{i}).y5 - min(bb_sel.(fields_{i}).y5)) ./ ...
        (max(bb_sel.(fields_{i}).y5) - min(bb_sel.(fields_{i}).y5));
    bb_sel.(fields_{i}).w5_st = ...
        (bb_sel.(fields_{i}).w5 - min(bb_sel.(fields_{i}).w5)) ./ ...
        (max(bb_sel.(fields_{i}).w5) - min(bb_sel.(fields_{i}).w5));
    bb_sel.(fields_{i}).h5_st = ...
        (bb_sel.(fields_{i}).h5 - min(bb_sel.(fields_{i}).h5)) ./ ...
        (max(bb_sel.(fields_{i}).h5) - min(bb_sel.(fields_{i}).h5));
    bb_sel.(fields_{i}).centerdist_st = ...
        (bb_sel.(fields_{i}).centerdist - min(bb_sel.(fields_{i}).centerdist)) ./ ...
        (max(bb_sel.(fields_{i}).centerdist) - min(bb_sel.(fields_{i}).centerdist));
    bb_sel.(fields_{i}).area_st = ...
        (bb_sel.(fields_{i}).area - min(bb_sel.(fields_{i}).area)) ./ ...
        (max(bb_sel.(fields_{i}).area) - min(bb_sel.(fields_{i}).area));
    bb_sel.(fields_{i}).whratio_st = ...
        (bb_sel.(fields_{i}).whratio - min(bb_sel.(fields_{i}).whratio)) ./ ...
        (max(bb_sel.(fields_{i}).whratio) - min(bb_sel.(fields_{i}).whratio));
    bb_sel.(fields_{i}).circum_st = ...
        (bb_sel.(fields_{i}).circum - min(bb_sel.(fields_{i}).circum)) ./ ...
        (max(bb_sel.(fields_{i}).circum) - min(bb_sel.(fields_{i}).circum));

    
    f = figure('Units','centimeters','OuterPosition',[52 -2 35 20]);
    hold on; box on; xlabel('frame'); ylabel('standardised bb property')
    title(fields_{i}, 'Interpreter','none');
    
    % plot GS behavior end and begin
    nr = str2double(regexp(fields_{i},'\d*','Match'));
    if contains(fields_{i},'up')
        framestart = gs.getup.fb(gs.getup.nr == nr);
        frameend = gs.getup.fe(gs.getup.nr == nr);
    else
        framestart = gs.liedown.fb(gs.liedown.nr == nr);
        frameend = gs.liedown.fe(gs.liedown.nr == nr);
    end
    
    % plot GS behavior end and begin
    plot([framestart framestart],[-0.5 1.5],'r--','LineWidth',2.5)
    plot([frameend frameend],[-0.5 1.5],'r--','LineWidth',2.5)

    % plot standardized traits
    x = bb_sel.(fields_{i}).frame;
    plot(x,bb_sel.(fields_{i}).x5_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[153 0 0]./255)
    plot(x,bb_sel.(fields_{i}).y5_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[252 158 17]./255)
    plot(x,bb_sel.(fields_{i}).w5_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[102 204 0]./255)
    plot(x,bb_sel.(fields_{i}).h5_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 153 153]./255)
    plot(x,bb_sel.(fields_{i}).centerdist_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 255]./255)
    plot(x,bb_sel.(fields_{i}).area_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[102 0 204]./255)
    plot(x,bb_sel.(fields_{i}).whratio_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[255 51 153]./255)
    plot(x,bb_sel.(fields_{i}).circum_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[255 153 153]./255)
    legend({'framestart','frameend',...
            'x','y','w','h','centerdist','area','whratio','circum'})
    
    ylim([-0.25 1.25])
    
    saveas(f,[init_.resdir 'f_smooth_' fields_{i} 'standard_bbprop.tif' ]);
    close all
end

% clear variables
clear f frameend framestart h5 i j nr w5 x x5 y y5 

%% derivatives
% calculate and plot derivatives
close all
clear pks
for i = 1:length(fields_)
    
    % min-max standardisation - (val-min)/(max-min)
    bb_sel.(fields_{i}).dif_x5_st = [NaN; diff(bb_sel.(fields_{i}).x5_st)];
    bb_sel.(fields_{i}).dif_y5_st = [NaN; diff(bb_sel.(fields_{i}).y5_st)];
    bb_sel.(fields_{i}).dif_h5_st = [NaN; diff(bb_sel.(fields_{i}).h5_st)];
    bb_sel.(fields_{i}).dif_w5_st = [NaN; diff(bb_sel.(fields_{i}).w5_st)];
    bb_sel.(fields_{i}).dif_cd_st = [NaN; diff(bb_sel.(fields_{i}).centerdist_st)];
    bb_sel.(fields_{i}).dif_area_st = [NaN; diff(bb_sel.(fields_{i}).area_st)];
    bb_sel.(fields_{i}).dif_whratio_st = [NaN; diff(bb_sel.(fields_{i}).whratio_st)];
    bb_sel.(fields_{i}).dif_circum_st = [NaN; diff(bb_sel.(fields_{i}).circum_st)];
  
       
    f = figure('Units','centimeters','OuterPosition',[52 -2 35 20]);
    hold on; box on; xlabel('frame'); ylabel('standardised bb property')
    title(fields_{i}, 'Interpreter','none');
    plot([0 500],[0 0],'k--')
    
    % plot GS behavior end and begin
    nr = str2double(regexp(fields_{i},'\d*','Match'));
    if contains(fields_{i},'up')
        framestart = gs.getup.fb(gs.getup.nr == nr);
        frameend = gs.getup.fe(gs.getup.nr == nr);
    else
        framestart = gs.liedown.fb(gs.liedown.nr == nr);
        frameend = gs.liedown.fe(gs.liedown.nr == nr);
    end
    
    % plot GS behavior end and begin
    plot([framestart framestart],[-2.5 2.5],'r:','LineWidth',2.5)
    plot([frameend frameend],[-2.5 2.5],'r:','LineWidth',2.5)

    % plot first differential
    x = bb_sel.(fields_{i}).frame;

    plot(x,bb_sel.(fields_{i}).dif_cd_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 255]./255)
    plot(x,bb_sel.(fields_{i}).dif_whratio_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[102 102 255]./255)
    plot(x,bb_sel.(fields_{i}).dif_circum_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 102 102]./255)
    
    % add legend and tune axes
    legend({'','framestart','frameend',...
            'centerdist','whratio','circum'},'AutoUpdate','off')
    ylim([-0.15 0.15]); xlim([0 460])
    
%-------------------------------------------------------------------------
    % find peaks MAX and plot
    [pks.(fields_{i}).dif_cd(:,1),pks.(fields_{i}).dif_cd(:,2)] = ...
            findpeaks(bb_sel.(fields_{i}).dif_cd_st,bb_sel.(fields_{i}).frame,...
            'MinPeakDistance',20,'MinPeakProminence',0.02,'NPeaks',30);
     plot(pks.(fields_{i}).dif_cd(:,2),pks.(fields_{i}).dif_cd(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
    [pks.(fields_{i}).dif_whratio(:,1),pks.(fields_{i}).dif_whratio(:,2)] = ...
            findpeaks(bb_sel.(fields_{i}).dif_whratio_st,bb_sel.(fields_{i}).frame,...
            'MinPeakDistance',20,'MinPeakProminence',0.02,'NPeaks',30);
     plot(pks.(fields_{i}).dif_whratio(:,2),pks.(fields_{i}).dif_whratio(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
    [pks.(fields_{i}).dif_circum(:,1),pks.(fields_{i}).dif_circum(:,2)] = ...
            findpeaks(bb_sel.(fields_{i}).dif_circum_st,bb_sel.(fields_{i}).frame,...
            'MinPeakDistance',20,'MinPeakProminence',0.02,'NPeaks',30);
     plot(pks.(fields_{i}).dif_circum(:,2),pks.(fields_{i}).dif_circum(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
   % find peaks MIN and plot
     [pks.(fields_{i}).dif_cd_min(:,1),pks.(fields_{i}).dif_cd_min(:,2)] = ...
            findpeaks(-bb_sel.(fields_{i}).dif_cd_st,bb_sel.(fields_{i}).frame,...
            'MinPeakDistance',20,'MinPeakProminence',0.02,'NPeaks',30);
     plot(pks.(fields_{i}).dif_cd_min(:,2),-pks.(fields_{i}).dif_cd_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
    [pks.(fields_{i}).dif_whratio_min(:,1),pks.(fields_{i}).dif_whratio_min(:,2)] = ...
            findpeaks(-bb_sel.(fields_{i}).dif_whratio_st,bb_sel.(fields_{i}).frame,...
            'MinPeakDistance',20,'MinPeakProminence',0.02,'NPeaks',30);
     plot(pks.(fields_{i}).dif_whratio_min(:,2),-pks.(fields_{i}).dif_whratio_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
    [pks.(fields_{i}).dif_circum_min(:,1),pks.(fields_{i}).dif_circum_min(:,2)] = ...
            findpeaks(-bb_sel.(fields_{i}).dif_circum_st,bb_sel.(fields_{i}).frame,...
            'MinPeakDistance',20,'MinPeakProminence',0.02,'NPeaks',30);
     plot(pks.(fields_{i}).dif_circum_min(:,2),-pks.(fields_{i}).dif_circum_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)     
                  
                  
    %-------------------------------------------------------------------%     
    % combine with plots standardized time series                       %
    %   assumption: centerdist remains +/- constant before standing up  %
    %               and after lying down                                %
    %               whratio and circum idem                             %
    %-------------------------------------------------------------------%
    
    
    
    yyaxis right; ylim([-2 1.25])
    plot(x, bb_sel.(fields_{i}).centerdist_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 255]./255)
    plot(x,bb_sel.(fields_{i}).whratio_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[102 102 255]./255)
    plot(x,bb_sel.(fields_{i}).circum_st,...
            'o-','LineWidth',1.2,'MarkerSize',3,'Color',[0 102 102]./255)
    
    
 
    % save figures
    saveas(f,[init_.resdir 'f_smooth_' fields_{i} 'firstdif.tif' ]);
end


%% last visualisation test - ratio of centerdist, whratio and circumference
% calculate and plot derivatives
close all
for i = 1:length(fields_)
    
    % ratios
    bb_sel.(fields_{i}).rat_cd_wh = ...
        (bb_sel.(fields_{i}).centerdist_st+1) ./ (bb_sel.(fields_{i}).whratio_st+1);
    bb_sel.(fields_{i}).rat_cd_circ = ...
        (bb_sel.(fields_{i}).centerdist_st+1) ./ (bb_sel.(fields_{i}).circum_st+1);
    bb_sel.(fields_{i}).rat_circ_wh = ...
        (bb_sel.(fields_{i}).circum_st+1) ./ (bb_sel.(fields_{i}).whratio_st+1);

    % prepare figures
    f = figure('Units','centimeters','OuterPosition',[52 -3 35 25]);
    subplot(3,1,1);hold on; box on; xlabel('frame'); ylabel('ratios')
    title(fields_{i}, 'Interpreter','none');
    plot([0 500],[1 1],'k--')
    
    % plot GS behavior end and begin
    nr = str2double(regexp(fields_{i},'\d*','Match'));
    if contains(fields_{i},'up')
        framestart = gs.getup.fb(gs.getup.nr == nr);
        frameend = gs.getup.fe(gs.getup.nr == nr);
    else
        framestart = gs.liedown.fb(gs.liedown.nr == nr);
        frameend = gs.liedown.fe(gs.liedown.nr == nr);
    end
    
    % plot GS behavior end and begin
    plot([framestart framestart],[0 2],'r:','LineWidth',2.5)
    plot([frameend frameend],[0 2],'r:','LineWidth',2.5)
    
    % plot standardized traits
    x = bb_sel.(fields_{i}).frame;
    plot(x,bb_sel.(fields_{i}).rat_cd_wh,...
            '-','LineWidth',1.2,'Color',[0 0 255]./255)
    plot(x,bb_sel.(fields_{i}).rat_cd_circ,...
            '-','LineWidth',1.2,'Color',[102 102 255]./255)
    plot(x,bb_sel.(fields_{i}).rat_circ_wh,...
            '-','LineWidth',1.2,'Color',[0 102 102]./255)
    
    % add legend and tune axes
    legend({'','framestart','frameend',...
            'cd-wh','cd-circ','circ-wh'},'AutoUpdate','off')
%     ylim([-0.15 0.15]); 
    xlim([0 460])
    
    % find peaks in ratios
    [pks.(fields_{i}).rat_cd_wh(:,1),pks.(fields_{i}).rat_cd_wh(:,2)] = ...
        findpeaks(bb_sel.(fields_{i}).rat_cd_wh,bb_sel.(fields_{i}).frame,...
        'MinPeakDistance',20,'MinPeakProminence',0.2,'NPeaks',5);
     plot(pks.(fields_{i}).rat_cd_wh(:,2),pks.(fields_{i}).rat_cd_wh(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
    [pks.(fields_{i}).rat_cd_circ(:,1),pks.(fields_{i}).rat_cd_circ(:,2)] = ...
        findpeaks(bb_sel.(fields_{i}).rat_cd_circ,bb_sel.(fields_{i}).frame,...
        'MinPeakDistance',20,'MinPeakProminence',0.2,'NPeaks',5);
     plot(pks.(fields_{i}).rat_cd_circ(:,2),pks.(fields_{i}).rat_cd_circ(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
    [pks.(fields_{i}).rat_circ_wh(:,1),pks.(fields_{i}).rat_circ_wh(:,2)] = ...
        findpeaks(bb_sel.(fields_{i}).rat_circ_wh,bb_sel.(fields_{i}).frame,...
        'MinPeakDistance',20,'MinPeakProminence',0.2,'NPeaks',5);
     plot(pks.(fields_{i}).rat_circ_wh(:,2),pks.(fields_{i}).rat_circ_wh(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
         [pks.(fields_{i}).rat_cd_wh(:,1),pks.(fields_{i}).rat_cd_wh(:,2)] = ...
        findpeaks(bb_sel.(fields_{i}).rat_cd_wh,bb_sel.(fields_{i}).frame,...
        'MinPeakDistance',20,'MinPeakProminence',0.2,'NPeaks',5);
    % local minima
    [pks.(fields_{i}).rat_cd_wh_min(:,1),pks.(fields_{i}).rat_cd_wh_min(:,2)] = ...
        findpeaks(-bb_sel.(fields_{i}).rat_cd_wh,bb_sel.(fields_{i}).frame,...
        'MinPeakDistance',20,'MinPeakProminence',0.2,'NPeaks',5);
    plot(pks.(fields_{i}).rat_cd_wh_min(:,2),-pks.(fields_{i}).rat_cd_wh_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
    [pks.(fields_{i}).rat_cd_circ_min(:,1),pks.(fields_{i}).rat_cd_circ_min(:,2)] = ...
        findpeaks(-bb_sel.(fields_{i}).rat_cd_circ,bb_sel.(fields_{i}).frame,...
        'MinPeakDistance',20,'MinPeakProminence',0.2,'NPeaks',5);
     plot(pks.(fields_{i}).rat_cd_circ_min(:,2),-pks.(fields_{i}).rat_cd_circ_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
    [pks.(fields_{i}).rat_circ_wh_min(:,1),pks.(fields_{i}).rat_circ_wh_min(:,2)] = ...
        findpeaks(-bb_sel.(fields_{i}).rat_circ_wh,bb_sel.(fields_{i}).frame,...
        'MinPeakDistance',20,'MinPeakProminence',0.2,'NPeaks',5);
     plot(pks.(fields_{i}).rat_circ_wh_min(:,2),-pks.(fields_{i}).rat_circ_wh_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)        
    %-------------------------------------------------------------------%     
    % combine with plots standardized time series                       %
    %   assumption: centerdist remains +/- constant before standing up  %
    %               and after lying down                                %
    %               whratio and circum idem                             %
    %-------------------------------------------------------------------%
    
    
    
    subplot(3,1,2);ylim([-0.25 1.25])
    hold on; box on; xlabel('frame'); ylabel('st pars')
    title(fields_{i}, 'Interpreter','none');
    plot([0 500],[0 0],'k--')
    
    % plot GS behavior end and begin
    plot([framestart framestart],[-0.25 1.25],'r:','LineWidth',2.5)
    plot([frameend frameend],[-0.25 1.25],'r:','LineWidth',2.5)
    
    % plot metrics
    plot(x, bb_sel.(fields_{i}).centerdist_st,...
            '-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 255]./255)
    plot(x,bb_sel.(fields_{i}).whratio_st,...
            '-','LineWidth',1.2,'MarkerSize',3,'Color',[102 102 255]./255)
    plot(x,bb_sel.(fields_{i}).circum_st,...
            '-','LineWidth',1.2,'MarkerSize',3,'Color',[0 102 102]./255)
    
    legend({'','framestart','frameend',...
            'cd','wh','circ'},'AutoUpdate','off')
    xlim([0 460])
    
    
    % plot first derivative
    subplot(3,1,3);ylim([-0.15 0.15])
    hold on; box on; xlabel('frame'); ylabel('derivatives')
    title(['derivative, ' fields_{i}], 'Interpreter','none');
    plot([0 500],[0 0],'k--'); xlim([0 460])
    
    % plot GS behavior end and begin
    plot([framestart framestart],[-0.5 2],'r:','LineWidth',2.5)
    plot([frameend frameend],[-0.5 2],'r:','LineWidth',2.5)
    

    % plt derivatives
    x = bb_sel.(fields_{i}).frame;

    plot(x,bb_sel.(fields_{i}).dif_cd_st,...
            '-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 255]./255)
    plot(x,bb_sel.(fields_{i}).dif_whratio_st,...
            '-','LineWidth',1.2,'MarkerSize',3,'Color',[102 102 255]./255)
    plot(x,bb_sel.(fields_{i}).dif_circum_st,...
            '-','LineWidth',1.2,'MarkerSize',3,'Color',[0 102 102]./255)
    
    % add legend and tune axes
    legend({'','framestart','frameend',...
            'centerdist','whratio','circum'},'AutoUpdate','off')
    ylim([-0.15 0.15]); xlim([0 460])
    
     % plot peaks
     plot(pks.(fields_{i}).dif_cd(:,2),pks.(fields_{i}).dif_cd(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
     plot(pks.(fields_{i}).dif_whratio(:,2),pks.(fields_{i}).dif_whratio(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
     plot(pks.(fields_{i}).dif_circum(:,2),pks.(fields_{i}).dif_circum(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
     plot(pks.(fields_{i}).dif_cd_min(:,2),-pks.(fields_{i}).dif_cd_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
     plot(pks.(fields_{i}).dif_whratio_min(:,2),-pks.(fields_{i}).dif_whratio_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)
     plot(pks.(fields_{i}).dif_circum_min(:,2),-pks.(fields_{i}).dif_circum_min(:,1),...
                      'rx','MarkerSize',6,'LineWidth',1.2)     

    
 
    % save figures
    saveas(f,[init_.resdir 'f_smooth_' fields_{i} 'ratios.tif' ]);
end

% % % % test changepoints based on std diff
% % % for i = 1:length(fields_)
% % %     figure;
% % %     
% % %     findchangepts(bb_sel.(fields_{i}){2:end,[31 33 34]}','Statistic','std')
% % %     
% % %     subplot(2,1,2); hold on;
% % %         nr = str2double(regexp(fields_{i},'\d*','Match'));
% % %     if contains(fields_{i},'up')
% % %         framestart = gs.getup.fb(gs.getup.nr == nr);
% % %         frameend = gs.getup.fe(gs.getup.nr == nr);
% % %     else
% % %         framestart = gs.liedown.fb(gs.liedown.nr == nr);
% % %         frameend = gs.liedown.fe(gs.liedown.nr == nr);
% % %     end
% % %     
% % %     % plot GS behavior end and begin
% % %     plot([framestart framestart],[0 2],'r:','LineWidth',2.5)
% % %     plot([frameend frameend],[0 2],'r:','LineWidth',2.5)
% % % end
% % % 
% % % close all

%% change per 1/3 second = 5 frames from derivative

for i = 1:length(fields_)
    % prepare figure
    f = figure('Units','centimeters','OuterPosition',[52 -3 35 20]);
    hold on; box on; 
    nr = str2double(regexp(fields_{i},'\d*','Match'));
    if contains(fields_{i},'up')
        framestart = gs.getup.fb(gs.getup.nr == nr);
        frameend = gs.getup.fe(gs.getup.nr == nr);
    else
        framestart = gs.liedown.fb(gs.liedown.nr == nr);
        frameend = gs.liedown.fe(gs.liedown.nr == nr);
    end
    
    % plot GS behavior end and begin
    plot([framestart framestart],[-1 2],'r:','LineWidth',2.5)
    plot([frameend frameend],[-1 2],'r:','LineWidth',2.5)
    
    plot(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).centerdist_st,...
               '-','LineWidth',1.2,'MarkerSize',3,'Color',[0 0 255]./255)
    plot(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).whratio_st,...
                    '-','LineWidth',1.2,'MarkerSize',3,'Color',[102 102 255]./255)
    plot(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).circum_st,...
                    '-','LineWidth',1.2,'MarkerSize',3,'Color',[0 102 102]./255)
    

    % fit polynomial and find nulpunten of derivatives -- CD
    p = polyfit(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).centerdist_st,6);
    x1 = 1:5:460;
    y1 = polyval(p,x1);
    dy1 = diff(y1);    
    idx = find(dy1(1:end-1)>0 & dy1(2:end) < 0);
    deriv.(fields_{i}).first_centerdist = x1(idx);
    dy2 = diff(dy1);
    idx = find(dy2(1:end-1)>0 & dy2(2:end) < 0);
    deriv.(fields_{i}).second_centerdist =x1(idx);
    plot(x1,y1,'k','LineWidth',1.3)
    
    % fit polynomial and find nulpunten of derivatives -- CD
    p = polyfit(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).whratio_st,6);
    x1 = 1:5:460;
    y1 = polyval(p,x1);
    dy1 = diff(y1);    
    idx = find(dy1(1:end-1)>0 & dy1(2:end) < 0);
    deriv.(fields_{i}).first_whratio = x1(idx);
    dy2 = diff(dy1);
    idx = find(dy2(1:end-1)>0 & dy2(2:end) < 0);
    deriv.(fields_{i}).second_whratio = x1(idx);
    plot(x1,y1,'k','LineWidth',1.3)
     
     % fit polynomial and find nulpunten of derivatives -- 
    p = polyfit(bb_sel.(fields_{i}).frame,bb_sel.(fields_{i}).circum_st,6);
    x1 = 1:5:460;
    y1 = polyval(p,x1);
    dy1 = diff(y1);    
    idx = find(dy1(1:end-1)>0 & dy1(2:end) < 0);
    deriv.(fields_{i}).first_circum = x1(idx);
    dy2 = diff(dy1);
    idx = find(dy2(1:end-1)>0 & dy2(2:end) < 0);
    deriv.(fields_{i}).second_circum = x1(idx);
    plot(x1,y1,'k','LineWidth',1.3)
    
    ylim([-0.1 1.3])
        
    saveas(f,[init_.resdir 'f_smooth_' fields_{i} 'derivatives.tif' ]);

end
close all



%% STEP 3: propose automated detection criteria
% test criteria / benchmark:
%       distance (in frames) to frame start and frame end
%
% parameters included (based on smooth x,y,w,h):
%       centerdistance
%       whratio        
%       circumference
%
% criteria to test:
%       max and min of first derivatives (peaks)
%       
%
%       

% prepare new array 'results array
sel_nrs.field = fields_;



% calculate
for i = 1:length(fields_)
    
    % start and end
    nr = str2double(regexp(fields_{i},'\d*','Match'));
    if contains(fields_{i},'up')
        sel_nrs.fs(i) = gs.getup.fb(gs.getup.nr == nr);
        sel_nrs.fe(i) = gs.getup.fe(gs.getup.nr == nr);
    else
        sel_nrs.fs(i) = gs.liedown.fb(gs.liedown.nr == nr);
        sel_nrs.fe(i) = gs.liedown.fe(gs.liedown.nr == nr);
    end
    
    % peaks of derivatives -- remark = not frames but index
    [~,sel_nrs.max_dif_cd(i)] = max(abs(bb_sel.(fields_{i}).dif_cd_st));
    [~,sel_nrs.max_dif_whratio(i)] = max(abs(bb_sel.(fields_{i}).dif_whratio_st));
    [~,sel_nrs.max_dif_circum(i)] = max(abs(bb_sel.(fields_{i}).dif_circum_st));
    
    % if getup - last peak of ratios // if lie down - first peak of ratios
    peaks1 = [pks.(fields_{i}).rat_cd_wh; pks.(fields_{i}).rat_cd_wh_min];
    peaks2 = [pks.(fields_{i}).rat_cd_circ; pks.(fields_{i}).rat_cd_circ_min];
    peaks3 = [pks.(fields_{i}).rat_circ_wh; pks.(fields_{i}).rat_circ_wh_min];
    
    if sel_nrs.updown(i) == 1 % up
        try;sel_nrs.ratiopeak_cd_wh(i) = max(peaks1(:,2));end
        try;sel_nrs.ratiopeak_cd_circ(i) = max(peaks2(:,2));end
        try;sel_nrs.ratiopeak_circ_wh(i) = max(peaks3(:,2));end
    else
        try;sel_nrs.ratiopeak_cd_wh(i) = min(peaks1(:,2)); end
        try;sel_nrs.ratiopeak_cd_circ(i) = min(peaks2(:,2));end
        try;sel_nrs.ratiopeak_circ_wh(i) = min(peaks3(:,2));end
    end 
    
    % based on the changepoints in derivatives -- std
    sel_nrs.cpts_der(i) = findchangepts(bb_sel.(fields_{i}){2:end,[31 33 34]}','Statistic','std');

    % based on the changepoints in ratios -- std
    sel_nrs.cpts_rat(i) = findchangepts(bb_sel.(fields_{i}){2:end,[35 36 37]}','Statistic','std');

    % based on first deriv -- up for first max
    if sel_nrs.updown(i) ~= 3 % up
        try;sel_nrs.der1_cd(i) = deriv.(fields_{i}).first_centerdist(1);end
        try;sel_nrs.der1_whratio(i) = deriv.(fields_{i}).first_whratio(1);end
        try;sel_nrs.der1_circum(i) = deriv.(fields_{i}).first_circum(1);end
    else
        try;sel_nrs.der1_cd(i) = deriv.(fields_{i}).first_centerdist(end);end
        try;sel_nrs.der1_whratio(i) = deriv.(fields_{i}).first_whratio(end);end
        try;sel_nrs.der1_circum(i) = deriv.(fields_{i}).first_circum(end);end
    end 
    
    % based on bending points
    if sel_nrs.updown(i) ~= 3 % up
        try;sel_nrs.der2_cd(i) = deriv.(fields_{i}).second_centerdist(1);end
        try;sel_nrs.der2_whratio(i) = deriv.(fields_{i}).second_whratio(1);end
        try;sel_nrs.der2_circum(i) = deriv.(fields_{i}).second_circum(1);end
    else
        try;sel_nrs.der2_cd(i) = deriv.(fields_{i}).second_centerdist(end);end
        try;sel_nrs.der2_whratio(i) = deriv.(fields_{i}).second_whratio(end);end
        try;sel_nrs.der2_circum(i) = deriv.(fields_{i}).second_circum(end);end
    end 
    
end


% if zeros - make NaN
sel_nrs.ratiopeak_cd_wh(sel_nrs.ratiopeak_cd_wh == 0) = NaN;
sel_nrs.ratiopeak_cd_circ(sel_nrs.ratiopeak_cd_circ == 0) = NaN;
sel_nrs.ratiopeak_circ_wh(sel_nrs.ratiopeak_circ_wh == 0) = NaN;
sel_nrs.der1_cd(sel_nrs.der1_cd == 0) = NaN;
sel_nrs.der1_whratio(sel_nrs.der1_whratio == 0) = NaN;
sel_nrs.der1_circum(sel_nrs.der1_circum == 0) = NaN;
sel_nrs.der2_cd(sel_nrs.der2_cd == 0) = NaN;
sel_nrs.der2_whratio(sel_nrs.der2_whratio == 0) = NaN;
sel_nrs.der2_circum(sel_nrs.der2_circum == 0) = NaN;




% 
N = length(fields_);

sel_nrs{N+1,9:end} = nanmean(abs(sel_nrs{1:N,9:end}-sel_nrs.fs(1:N)));  % distance to frame start average
sel_nrs{N+2,9:end} = nanmean(abs(sel_nrs{1:N,9:end}-sel_nrs.fe(1:N)));  % distance to frame start average
sel_nrs{N+3,9:end} = sum(abs(sel_nrs{1:N,9:end}-sel_nrs.fs(1:N))<30);  % distance to frame start average
sel_nrs{N+4,9:end} = sum(abs(sel_nrs{1:N,9:end}-sel_nrs.fe(1:N))<30);  % distance to frame start average
sel_nrs{N+5,9:end} = sum(abs(sel_nrs{1:N,9:end}-sel_nrs.fs(1:N))<75);  % distance to frame start average
sel_nrs{N+6,9:end} = sum(abs(sel_nrs{1:N,9:end}-sel_nrs.fe(1:N))<75);  % distance to frame start average

sel_nrs{N+1,6} = {'avg_abs_fdist_fs'};
sel_nrs{N+2,6} = {'avg_abs_fdist_fe'};
sel_nrs{N+3,6} = {'n_less_2s_fs'};
sel_nrs{N+4,6} = {'n_less_2s_fe'};
sel_nrs{N+5,6} = {'n_less_5s_fs'};
sel_nrs{N+6,6} = {'n_less_5s_fe'};

clear ans dy1 dy2 f frameend framestart i idx N nr p peaks1 peaks2 peaks3
clear peaks p x x1 y1 temp 

% save([init_.datadir 'D1_bb_results.mat'])
save([init_.datadir 'D1_bb_results_it2.mat'])

%% add metrics separate up down
% load([init_.datadir 'iteration1\D1_bb_results.mat'])
% load([init_.datadir 'D1_bb_results_it2.mat'])

K = length(fields_);
% up separately
idx = find(sel_nrs.updown == 1);
sel_nrs{K+7,9:end} = nanmean(abs(sel_nrs{idx,9:end}-sel_nrs.fs(idx)));  % distance to frame start average
sel_nrs{K+8,9:end} = nanmean(abs(sel_nrs{idx,9:end}-sel_nrs.fe(idx)));  % distance to frame start average
sel_nrs{K+9,9:end} = nansum(abs(sel_nrs{idx,9:end}-sel_nrs.fs(idx))<30);  % distance to frame start average
sel_nrs{K+10,9:end} = nansum(abs(sel_nrs{idx,9:end}-sel_nrs.fe(idx))<30);  % distance to frame start average
sel_nrs{K+11,9:end} = nansum(abs(sel_nrs{idx,9:end}-sel_nrs.fs(idx))<75);  % distance to frame start average
sel_nrs{K+12,9:end} = nansum(abs(sel_nrs{idx,9:end}-sel_nrs.fe(idx))<75);  % distance to frame start average

% down separately
idx = find(sel_nrs.updown == 0 & sel_nrs.nr ~= 0 );
sel_nrs{K+13,9:end} = nanmean(abs(sel_nrs{idx,9:end}-sel_nrs.fs(idx)));  % distance to frame start average
sel_nrs{K+14,9:end} = nanmean(abs(sel_nrs{idx,9:end}-sel_nrs.fe(idx)));  % distance to frame start average
sel_nrs{K+15,9:end} = nansum(abs(sel_nrs{idx,9:end}-sel_nrs.fs(idx))<30);  % distance to frame start average
sel_nrs{K+16,9:end} = nansum(abs(sel_nrs{idx,9:end}-sel_nrs.fe(idx))<30);  % distance to frame start average
sel_nrs{K+17,9:end} = nansum(abs(sel_nrs{idx,9:end}-sel_nrs.fs(idx))<75);  % distance to frame start average
sel_nrs{K+18,9:end} = nansum(abs(sel_nrs{idx,9:end}-sel_nrs.fe(idx))<75);  % distance to frame start average

sel_nrs{K+7,6} = {'avg_abs_fdist_fs_up'};
sel_nrs{K+8,6} = {'avg_abs_fdist_fe_up'};
sel_nrs{K+9,6} = {'n_less_2s_fs_up'};
sel_nrs{K+10,6} = {'n_less_2s_fe_up'};
sel_nrs{K+11,6} = {'n_less_5s_fs_up'};
sel_nrs{K+12,6} = {'n_less_5s_fe_up'};
sel_nrs{K+13,6} = {'avg_abs_fdist_fs_do'};
sel_nrs{K+14,6} = {'avg_abs_fdist_fe_do'};
sel_nrs{K+15,6} = {'n_less_2s_fs_do'};
sel_nrs{K+16,6} = {'n_less_2s_fe_do'};
sel_nrs{K+17,6} = {'n_less_5s_fs_do'};
sel_nrs{K+18,6} = {'n_less_5s_fe_do'};

% writetable(sel_nrs,'results.xlsx','Sheet','all')
writetable(sel_nrs,'results.xlsx','Sheet','all_it2')