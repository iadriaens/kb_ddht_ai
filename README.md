# DDHT AI
    Created by @iadriaens
    June 23, 2021

## Project description

Knowledge Base Digital Data - High Tech Artificial Intelligence (KB DDHT AI) aims to develop and validate tools for the automated detection and tracking of individual cows and characterization of their behavior based on imagery/video data.

In 2021, the target behaviors are ‘standing up’ and ‘lying down’, from which the duration might be linked to e.g. welfare and claw health. Additionally, a multiple cow-tracker will be implemented that allows to track animals throughout the barn.

This repository will contain the computing pipeline code modules for DDHT AI.

## Environment

I created a local environment via Anaconda for this project, can be requested via ines.adriaens@wur.nl. The environment configures Python 3.7, and I use Spyder as IDE.

## Codes

__Pre-processing__
1) Frame sampler  
2) Frame proprocessing  
3) Inventarisation  
4) Data selection  
5) Data splicing  
6) Annotation  
7) Data augmentation  


__Modelling__
1) Model / training
2) Cross-validation
3) Model evaluation and visualisation  


__Post-processing__
1) Tracking (ina)
2) Getting up / lying down

## Scripts



