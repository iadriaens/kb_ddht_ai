# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 08:47:17 2021

@author: adria036

This scripts samples frames based on "timing" in the video, starting from 
an excel file, and stores them as .jpg files in destination folder

> can be integrated in a later stage in S3_sampleframes

"""

#%% Load packages

import os
import numpy as np
import pandas as pd
import cv2
from datetime import date
from openpyxl import load_workbook
import random as rand


#%% define constants and filepaths

# source directory
#src = r'C:/Users/adria036/OneDrive - Wageningen University & ' \
#     + 'Research/iAdriaens_doc/Projects/cKamphuis/ddhtAI/data/vids/'
src = r'W:\ASG\WLR_Dataopslag\Genomica\Sustainable_breeding//' \
        '44 0000 2700 KB DDHT AI 2020\\6. data/annotation_videos/'

# file with information of the vids
vidfile = '20210801_videos_voor_annotatie.xlsx'

# destination directory
dst = src 
#r'C:/Users/adria036/OneDrive - Wageningen University & ' \
#     + 'Research/iAdriaens_doc/Projects/cKamphuis/ddhtAI/data/ann_frames/'

# create destination folder when it doesn't exists
if os.path.exists(dst) == False:
    os.mkdir(dst)
    
# read excel with meta data in pandas dataframe
vid_list = pd.read_excel(src+vidfile, skiprows=1, header=2, usecols = 'B:J')
vid_list.head()

# set number of frames to be sampled per video
nosamples = 10

# naming of the frames
samplename = []

for i in range(0,len(vid_list)):
    samplename.append("vid_" + str(i))     # append

# add samplename to vid_list
vid_list["samplename"] = samplename


#%% read videos and set framerate

no_frames = []                              # prepare inventory no frames
vid_length = []                             # prepare video length
frame_rate = []                             # prepare frame rate
frame_sel = []                              # prepare selected sample
vid_drop = []                               # prepare to drop
for i in range(0, len(vid_list)):
    file = vid_list.directory[i] + '\\' + vid_list.video[i]  # video path
    print("i = " + str(i)+ " of " + str(len(vid_list)-1))
    if os.path.exists(file):
        cap = cv2.VideoCapture(file)                      # capture video
    
        # video properties
        data = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))  # no of frames
        no_frames.append(data)                  # make list of no of frames
        data = int(cap.get(cv2.CAP_PROP_FPS))   # frames per second (framerate)
        frame_rate.append(data)                 # make list of fps
        data = no_frames[i]/frame_rate[i]       # calculate video length in sec
        vid_length.append(data)                 # make list of vid length
        
        if pd.notnull(vid_list["interest"][i]):
            # calculate frame to be sampled (excel => variable "interest" )
            dt = vid_list.interest[i]               # time object
            nseconds = dt.hour*3600 + \
                       dt.minute*60 + \
                       dt.second                    # no seconds in video
            data = nseconds * frame_rate[i]         # frame to sample
            frame_sel.append(data)                  # list
        else:
            data = np.nan
            frame_sel.append(data)
            
        # release videocapture
        cap.release()                           # release video capture
    else:
        vid_drop.append(i)                  # add to list to drop
        data = np.nan                       # add nan
        no_frames.append(data)                  # make list of no of frames
        frame_rate.append(data)                 # make list of fps
        frame_sel.append(data)
        vid_length.append(data) 
        
# add lists as columns to dataframe
vid_list["no_frames"] = no_frames
vid_list["vid_length"] = vid_length
vid_list["frame_rate"] = frame_rate
vid_list["frame_sel"] = frame_sel
    
# show vid_list first and last 10 lines
vid_list.head(10)
vid_list.tail(30)

# drop lines where vid_drop = 1
vid_list = vid_list.drop(labels=vid_drop)
vid_list = vid_list.reset_index()
vid_list = vid_list.drop(columns="index")

# del variables
del data, file, frame_rate, frame_sel, no_frames, nseconds, i
del vid_drop, vid_length


#%% sample frames and save with specified name, save xlsx with name and sample

# sample frames and save as .jpg
for i in range(0,len(vid_list)):
    if pd.notnull(vid_list["interest"][i]):
        # prepare video reading
        file = vid_list.directory[i] + '\\' + vid_list.video[i]  # video path
        cap = cv2.VideoCapture(file)                      # capture video
    
        # set frame name for writing + print
        samplename = dst + vid_list["samplename"][i] + \
                     "_" + "frame_" + "extra" + ".jpg"
        print(vid_list["samplename"][i] + "_" + "frame_" + "extra" + ".jpg")
        # set frame number that has to be read
        cap.set(cv2.CAP_PROP_POS_FRAMES, int(vid_list["frame_sel"][i]))
        ret, img = cap.read()                   # read frame
        cv2.imwrite(samplename, img)            # save sampled frame

        # release videocapture
        cap.release()
        
        # clear workspace
        del samplename, ret, img, file
        
# clear workspace
del i

#%% Sample frames based on random numbers using file_list

# define start and stop edges for selection of samples
edges = []
for i in range(0,len(vid_list)):
    arr = np.linspace(0, vid_list["no_frames"][i],
                      num=nosamples+1,
                      dtype = "int")
    edges.append(np.transpose(arr))

# add to fn dataframe
vid_list["edges"] = edges

# sample random numbers (frames) within edges per video
sampledframes = []
for i in range(0,len(vid_list)):
    frame = []
    for j in range(0,nosamples):
        sample = rand.randrange(vid_list["edges"][i][j],
                                vid_list["edges"][i][j+1])
        frame.append(sample)
    sampledframes.append(frame)
    
# add to fn dataframe
vid_list["sampledframes"] = sampledframes

# clear workspace memory
del arr, edges, i, sampledframes, j, sample, frame


#%% do the actual sampling and write vid_list to excel

for i in range(0,len(vid_list)):
    print(vid_list["video"][i])            # print filename read
    print("i = " + str(i)+ " of " + str(len(vid_list)-1))
    os.path.exists(vid_list["directory"][i] + "\\" + \
                           vid_list["video"][i])
    
    cap = cv2.VideoCapture(vid_list["directory"][i] + "\\" + \
                           vid_list["video"][i])  # capture video
    
    # read and write selected frames
    for j in range(0,nosamples):
        # set frame name for writing + print
        samplename = dst + vid_list["samplename"][i] + \
                     "_frame_" + str(j) + ".jpg"
        print(vid_list["samplename"][i] + "_frame_" + str(j) + ".jpg")
        # set frame number that has to be read
        cap.set(cv2.CAP_PROP_POS_FRAMES, vid_list["sampledframes"][i][j])
        # read frame
        ret, img = cap.read()
        # save sampled frame
        cv2.imwrite(samplename, img)

    # release videocapture
    cap.release()
    
    
#%% save vid_list with information on sampling in source file

# prepare saving -- today's date for record
today = date.today()
today = today.strftime("%Y%m%d")

# create destination folder when it doesn't exists
path = dst+"FrameSelection.xlsx"
if os.path.exists(path):
    # if it exists, load workbook and add to that
    book = load_workbook(path)
    options = {}
    options['strings_to_formulas'] = False
    options['strings_to_urls'] = False
    writer = pd.ExcelWriter(path, engine='openpyxl', options=options)
    writer.book = book
    vid_list.to_excel(writer, sheet_name = today)
    writer.save()
    writer.close()
else:
    vid_list.to_excel(path,sheet_name = today)
    
# save in separate sheet in the source file as well for records
path = src+vidfile
book = load_workbook(path)
options = {}
options['strings_to_formulas'] = False
options['strings_to_urls'] = False
writer = pd.ExcelWriter(path, engine = 'openpyxl', options=options)
writer.book = book
vid_list.to_excel(writer, sheet_name = today)
writer.save()
writer.close()

# save as .pkl in dst
vid_list.to_pickle(dst+"FrameSelection_"+today)
    
# clear workspace 
del i, img, nosamples, samplename, ret, j, today, path 

