# -*- coding: utf-8 -*-
"""
Created on Thu Jul 15 14:50:14 2021

@author: adria036

- Creates list of video files in folder
- Determine size and frames of each video
- Samples a predefined number of frames and write as .jpg to specified folder


"""

#%% Load packages
import os
import numpy as np
import pandas as pd
import cv2
import random as rand
from datetime import date


#%% define constants and filepaths

# source directory
src = r'C:/Users/adria036/OneDrive - Wageningen University & ' \
     + 'Research/iAdriaens_doc/Projects/cKamphuis/ddhtAI/data/vids/'

# destination directory
dst = r'C:/Users/adria036/OneDrive - Wageningen University & ' \
     + 'Research/iAdriaens_doc/Projects/cKamphuis/ddhtAI/data/ann_frames/'

# filenames in list with conditions as specified below
fn = []                                 # prepare fn
fn = [f for f in os.listdir(src) if os.path.isfile(src+f) \
        and (".mp4" in f or ".avi" in f) ] # vids with .mp4 or .avi format

# convert fn to pandas dataframe to store which frames are sampled
fn = pd.DataFrame(data = fn,
                  columns = ['file'])

# set number of frames to be sampled per video
nosamples = 5

#%% prepare outputs

# create destination folder when it doesn't exists
if os.path.exists(dst) == False:
    os.mkdir(dst)
    
# naming of the frames
samplename = []
for i in range(0,len(fn)):
    samplename.append("vid_" + str(i))     # append
    
# add samplename to fn
fn["samplename"] = samplename

# number of frames in each video
noframes = np.zeros((len(fn),1),dtype=int)
for i in range(0,len(fn)):
    vid = cv2.VideoCapture(src+fn["file"][i])
    noframes[i] = int(vid.get(cv2.CAP_PROP_FRAME_COUNT))
    vid.release()
    
# add noframes to fn
fn["noframes"] = noframes

# define start and stop ranges for selection of samples
edges = []
for i in range(0,len(fn)):
    arr = np.linspace(0, noframes[i][0],
                      num=nosamples+1,
                      dtype = "int")
    edges.append(np.transpose(arr))

# add to fn dataframe
fn["edges"] = edges

# sample random numbers (frames) within edges per video
sampledframes = []
for i in range(0,len(fn)):
    frame = []
    for j in range(0,nosamples):
        sample = rand.randrange(fn["edges"][i][j],fn["edges"][i][j+1])
        frame.append(sample)
    sampledframes.append(frame)
    
# add to fn dataframe
fn["sampledframes"] = sampledframes

# clear workspace memory
del arr, edges, i, sampledframes, j, sample, samplename, frame


#%% do the actual sampling and write fn to excel

for i in range(0,len(fn)):
    print(fn["file"][i])            # print filename read
    cap = cv2.VideoCapture(src+fn["file"][i])  # capture video
    
    # read and write selected frames
    for j in range(0,nosamples):
        # set frame name for writing + print
        samplename = dst + fn["samplename"][i] + \
                     "_" + "frame_" + str(j) + ".jpg"
        print(fn["samplename"][i] + "_" + "frame_" + str(j) + ".jpg")
        # set frame number that has to be read
        cap.set(cv2.CAP_PROP_POS_FRAMES, fn["sampledframes"][i][j])
        # read frame
        ret, img = cap.read()
        # save sampled frame
        cv2.imwrite(samplename, img) 

    # release videocapture
    cap.release()
    
# write metadata frames to excel file in dest
today = date.today()
today = today.strftime("%Y%m%d")
fn.to_excel(dst+"FrameSelection.xlsx",sheet_name = str(today))

# save as pkl
fn.to_pickle(dst+"SFrameSelection_"+today)

# clear workspace 
del i, img, noframes, nosamples, samplename, ret, j, today 
